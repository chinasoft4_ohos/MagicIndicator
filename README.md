# MagicIndicator

#### 项目介绍

- 项目名称：MagicIndicator
- 所属系列：openharmony的第三方组件适配移植
- 功能：可定制的和可扩展的 PageSlider 指示框架。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.7.0

#### 效果演示

<img src="gif/magicIndictor_demo.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
 }
 ```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:MagicIndicator:0.0.1-SNAPSHOT')
    ......  
 }
 ```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1、在 XML 文件中添加以下代码,添加 MagicIndicator 和 PageSlider 布局

```
    <net.lucode.hackware.magicindicator.MagicIndicator
        ohos:id="$+id:magic_indicator"
        ohos:height="45vp"
        ohos:width="match_parent"
        ohos:top_margin="5vp"/>

    <PageSlider
        ohos:id="$+id:page_slider"
        ohos:height="100vp"
        ohos:width="match_parent"/>
```

2、将代码添加到 java 文件中，初始化 MagicIndicator。

```
        MagicIndicator magicIndicator = (MagicIndicator) findComponentById(ResourceTable.Id_magic_indicator3);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                simplePagerTitleView.setNormalColor(Color.GRAY.getValue());
                simplePagerTitleView.setSelectedColor(Color.WHITE.getValue());
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mViewPager.setCurrentPage(index);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                linePagerIndicator.setColors(Color.WHITE.getValue());
                return linePagerIndicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
```
 绑定 PageSlider,页面切换时达到 Indicator 联动效果

```
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_page_slider);
        pageSlider.setProvider(mExamplePagerAdapter);
        ViewPagerHelper.bind(magicIndicator, pageSlider);
```
 或者使用 Fragment Container（通过 hide()、show() 切换 Fragment）
```
        FragmentContainerHelper fragmentContainerHelper = new FragmentContainerHelper();
        // ......
        // 切换时调用
        fragmentContainerHelper.handlePageSelected(index);
```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 0.0.1-SNAPSHOT

#### 版权和许可信息

```
MIT License

Copyright (c) 2016 hackware1993

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

