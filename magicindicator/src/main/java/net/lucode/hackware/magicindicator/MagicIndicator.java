package net.lucode.hackware.magicindicator;

import net.lucode.hackware.magicindicator.abs.IPagerNavigator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * 整个框架的入口
 *
 * @since 2020-09-29
 */
public class MagicIndicator extends StackLayout {
    private IPagerNavigator mNavigator;

    /**
     * MagicIndicator
     *
     * @param context context
     */
    public MagicIndicator(Context context) {
        super(context);
    }

    /**
     * MagicIndicator
     *
     * @param context context
     * @param attrs attrs
     */
    public MagicIndicator(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * onPageScrolled
     *
     * @param position position
     * @param positionOffset positionOffset
     * @param positionOffsetPixels positionOffsetPixels
     */
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mNavigator != null) {
            mNavigator.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }
    }

    /**
     * onPageSelected
     *
     * @param position position
     */
    public void onPageSelected(int position) {
        if (mNavigator != null) {
            mNavigator.onPageSelected(position);
        }
    }

    /**
     * onPageScrollStateChanged
     *
     * @param state state
     */
    public void onPageScrollStateChanged(int state) {
        if (mNavigator != null) {
            mNavigator.onPageScrollStateChanged(state);
        }
    }

    /**
     * getNavigator
     *
     * @return mNavigator
     */
    public IPagerNavigator getNavigator() {
        return mNavigator;
    }

    /**
     * setNavigator
     *
     * @param navigator navigator
     */
    public void setNavigator(IPagerNavigator navigator) {
        if (mNavigator == navigator) {
            return;
        }
        if (mNavigator != null) {
            mNavigator.onDetachFromMagicIndicator();
        }
        mNavigator = navigator;
        removeAllComponents();
        if (mNavigator instanceof Component) {
            LayoutConfig lp = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);
            addComponent((Component) mNavigator, lp);
            mNavigator.onAttachToMagicIndicator();
        }
    }
}
