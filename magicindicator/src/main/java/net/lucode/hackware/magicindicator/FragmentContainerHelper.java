package net.lucode.hackware.magicindicator;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.model.PositionData;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;

import java.util.ArrayList;
import java.util.List;

/**
 * 使得MagicIndicator在FragmentContainer中使用
 *
 * @since 2020-09-29
 */
public class FragmentContainerHelper {
    private static final int DURATION = 150;
    private List<MagicIndicator> mMagicIndicators = new ArrayList<MagicIndicator>();
    private AnimatorValue mScrollAnimator;
    private int mLastSelectedIndex;
    private int mDuration = DURATION;
    private int mInterpolator = Animator.CurveType.ACCELERATE_DECELERATE;
    private int currentPosition = 0;
    private int selectedPosition = 0;

    Animator.StateChangedListener mAnimatorListener = new Animator.StateChangedListener() {
        @Override
        public void onStart(Animator animator) {
        }

        @Override
        public void onStop(Animator animator) {
        }

        @Override
        public void onCancel(Animator animator) {
        }

        @Override
        public void onEnd(Animator animator) {
            dispatchPageScrollStateChanged(ScrollState.SCROLL_STATE_IDLE);
            mScrollAnimator = null;
        }

        @Override
        public void onPause(Animator animator) {
        }

        @Override
        public void onResume(Animator animator) {
        }
    };
    private AnimatorValue.ValueUpdateListener mAnimatorUpdateListener = new AnimatorValue.ValueUpdateListener() {
        @Override
        public void onUpdate(AnimatorValue animatorValue, float value) {
            float newValue = value;
            if (currentPosition < selectedPosition) {
                newValue = currentPosition + (selectedPosition - currentPosition) * value;
            } else if (currentPosition > selectedPosition) {
                newValue = currentPosition - (currentPosition - selectedPosition) * value;
            }
            float positionOffsetSum = newValue;
            int position = (int) positionOffsetSum;
            float positionOffset = positionOffsetSum - position;
            if (positionOffsetSum < 0) {
                position = position - 1;
                positionOffset = 1.0f + positionOffset;
            }
            dispatchPageScrolled(position, positionOffset, 0);
        }
    };

    /**
     * FragmentContainerHelper
     */
    public FragmentContainerHelper() {
    }

    /**
     * FragmentContainerHelper
     *
     * @param magicIndicator magicIndicator
     */
    public FragmentContainerHelper(MagicIndicator magicIndicator) {
        mMagicIndicators.add(magicIndicator);
    }

    /**
     * IPagerIndicator支持弹性效果的辅助方法
     *
     * @param positionDataList positionDataList
     * @param index index
     * @return PositionData
     */
    public static PositionData getImitativePositionData(List<PositionData> positionDataList, int index) {
        if (index >= 0 && index <= positionDataList.size() - 1) { // 越界后，返回假的PositionData
            return positionDataList.get(index);
        } else {
            PositionData result = new PositionData();
            PositionData referenceData;
            int offset;
            if (index < 0) {
                offset = index;
                referenceData = positionDataList.get(0);
            } else {
                offset = index - positionDataList.size() + 1;
                referenceData = positionDataList.get(positionDataList.size() - 1);
            }
            result.mLeft = referenceData.mLeft + offset * referenceData.width();
            result.mTop = referenceData.mTop;
            result.mRight = referenceData.mRight + offset * referenceData.width();
            result.mBottom = referenceData.mBottom;
            result.mContentLeft = referenceData.mContentLeft + offset * referenceData.width();
            result.mContentTop = referenceData.mContentTop;
            result.mContentRight = referenceData.mContentRight + offset * referenceData.width();
            result.mContentBottom = referenceData.mContentBottom;
            return result;
        }
    }

    /**
     * handlePageSelected
     *
     * @param selectedIndex selectedIndex
     */
    public void handlePageSelected(int selectedIndex) {
        handlePageSelected(selectedIndex, true);
    }

    /**
     * handlePageSelected
     *
     * @param selectedIndex selectedIndex
     * @param isSmooth isSmooth
     */
    public void handlePageSelected(int selectedIndex, boolean isSmooth) {
        if (mLastSelectedIndex == selectedIndex) {
            return;
        }
        if (isSmooth) {
            if (mScrollAnimator == null || !mScrollAnimator.isRunning()) {
                dispatchPageScrollStateChanged(ScrollState.SCROLL_STATE_SETTLING);
            }
            dispatchPageSelected(selectedIndex);
            if (mScrollAnimator != null) {
                mScrollAnimator.cancel();
                mScrollAnimator = null;
            }
            mScrollAnimator = new AnimatorValue();

            mScrollAnimator.setValueUpdateListener(mAnimatorUpdateListener);
            mScrollAnimator.setStateChangedListener(mAnimatorListener);
            mScrollAnimator.setCurveType(mInterpolator);
            mScrollAnimator.setDuration(mDuration);
            mScrollAnimator.start();
            currentPosition = mLastSelectedIndex;
            selectedPosition = selectedIndex;
        } else {
            dispatchPageSelected(selectedIndex);
            if (mScrollAnimator != null && mScrollAnimator.isRunning()) {
                dispatchPageScrolled(mLastSelectedIndex, 0.0f, 0);
            }
            dispatchPageScrollStateChanged(ScrollState.SCROLL_STATE_IDLE);
            dispatchPageScrolled(selectedIndex, 0.0f, 0);
        }
        mLastSelectedIndex = selectedIndex;
    }

    /**
     * setDuration
     *
     * @param duration duration
     */
    public void setDuration(int duration) {
        mDuration = duration;
    }

    /**
     * setInterpolator
     *
     * @param interpolator interpolator
     */
    public void setInterpolator(int interpolator) {
        mInterpolator = interpolator;
    }

    /**
     * attachMagicIndicator
     *
     * @param magicIndicator magicIndicator
     */
    public void attachMagicIndicator(MagicIndicator magicIndicator) {
        mMagicIndicators.add(magicIndicator);
    }

    private void dispatchPageSelected(int pageIndex) {
        for (MagicIndicator magicIndicator : mMagicIndicators) {
            magicIndicator.onPageSelected(pageIndex);
        }
    }

    private void dispatchPageScrollStateChanged(int state) {
        for (MagicIndicator magicIndicator : mMagicIndicators) {
            magicIndicator.onPageScrollStateChanged(state);
        }
    }

    private void dispatchPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        for (MagicIndicator magicIndicator : mMagicIndicators) {
            magicIndicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }
    }
}
