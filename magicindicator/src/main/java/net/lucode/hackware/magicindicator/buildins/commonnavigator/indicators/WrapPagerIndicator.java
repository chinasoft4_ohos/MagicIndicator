package net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators;

import net.lucode.hackware.magicindicator.FragmentContainerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.model.PositionData;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.List;

/**
 * 包裹住内容区域的指示器，类似天天快报的切换效果，需要和IMeasurablePagerTitleView配合使用
 *
 * @since 2020-09-29
 */
public class WrapPagerIndicator extends Component implements IPagerIndicator, Component.DrawTask {
    private int mVerticalPadding;
    private int mHorizontalPadding;
    private int mFillColor;
    private float mRoundRadius;
    private int mStartInterpolator = Animator.CurveType.LINEAR;
    private int mEndInterpolator = Animator.CurveType.LINEAR;

    private List<PositionData> mPositionDataList;
    private Paint mPaint;

    private RectFloat mRect = new RectFloat();
    private boolean mIsRoundRadiusSet;

    /**
     * WrapPagerIndicator
     *
     * @param context context
     */
    public WrapPagerIndicator(Context context) {
        super(context);
        init(context);
    }

    /**
     * WrapPagerIndicator
     *
     * @param context context
     * @param attrSet attrSet
     * @param verticalPadding mVerticalPadding
     */
    public WrapPagerIndicator(Context context, AttrSet attrSet, int verticalPadding) {
        super(context, attrSet);
        init(context);
    }

    /**
     * WrapPagerIndicator
     *
     * @param context context
     * @param attrSet attrSet
     * @param styleName styleName
     * @param verticalPadding mVerticalPadding
     */
    public WrapPagerIndicator(Context context, AttrSet attrSet, String styleName, int verticalPadding) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        mPaint = new Paint();
        mPaint.setSubpixelAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mVerticalPadding = UIUtil.vp2px(context, 6);
        mHorizontalPadding = UIUtil.vp2px(context, 10);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mPaint.setColor(new Color(mFillColor));
        canvas.drawRoundRect(mRect, mRoundRadius, mRoundRadius, mPaint);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mPositionDataList == null || mPositionDataList.isEmpty()) {
            return;
        }
        int nextPosition = position + 1;
        if (positionOffsetPixels < 0) {
            nextPosition = position - 1;
        }

        // 计算锚点位置
        PositionData current = FragmentContainerHelper.getImitativePositionData(mPositionDataList, position);
        PositionData next = FragmentContainerHelper.getImitativePositionData(mPositionDataList, nextPosition);

        mRect.left = current.mLeft + (next.mLeft - current.mLeft) * positionOffset;
        mRect.top = current.mContentTop - mVerticalPadding;
        mRect.right = current.mRight + (next.mRight - current.mRight) * positionOffset;
        mRect.bottom = current.mContentBottom + mVerticalPadding * 1.5f;

        if (!mIsRoundRadiusSet) {
            mRoundRadius = mRect.getHeight() / 2;
        }
        invalidate();
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPositionDataProvide(List<PositionData> dataList) {
        mPositionDataList = dataList;
    }

    /**
     * getPaint
     *
     * @return mPaint
     */
    public Paint getPaint() {
        return mPaint;
    }

    /**
     * getVerticalPaddings
     *
     * @return mVerticalPadding
     */
    public int getVerticalPaddings() {
        return mVerticalPadding;
    }

    /**
     * setVerticalPadding
     *
     * @param verticalPadding verticalPadding
     */
    public void setVerticalPadding(int verticalPadding) {
        mVerticalPadding = verticalPadding;
    }

    /**
     * getHorizontalPaddings
     *
     * @return mHorizontalPadding
     */
    public int getHorizontalPaddings() {
        return mHorizontalPadding;
    }

    /**
     * setHorizontalPadding
     *
     * @param horizontalPadding horizontalPadding
     */
    public void setHorizontalPadding(int horizontalPadding) {
        mHorizontalPadding = horizontalPadding;
    }

    /**
     * getFillColor
     *
     * @return mFillColor
     */
    public int getFillColor() {
        return mFillColor;
    }

    /**
     * setFillColor
     *
     * @param fillColor fillColor
     */
    public void setFillColor(int fillColor) {
        mFillColor = fillColor;
    }

    /**
     * getRoundRadius
     *
     * @return mRoundRadius
     */
    public float getRoundRadius() {
        return mRoundRadius;
    }

    /**
     * setRoundRadius
     *
     * @param roundRadius roundRadius
     */
    public void setRoundRadius(float roundRadius) {
        mRoundRadius = roundRadius;
        mIsRoundRadiusSet = true;
    }

    /**
     * getStartInterpolator
     *
     * @return mStartInterpolator
     */
    public int getStartInterpolator() {
        return mStartInterpolator;
    }

    /**
     * setStartInterpolator
     *
     * @param startInterpolator startInterpolator
     */
    public void setStartInterpolator(int startInterpolator) {
        mStartInterpolator = startInterpolator;
    }

    /**
     * getEndInterpolator
     *
     * @return mEndInterpolator
     */
    public int getEndInterpolator() {
        return mEndInterpolator;
    }

    /**
     * setEndInterpolator
     *
     * @param endInterpolator endInterpolator
     */
    public void setEndInterpolator(int endInterpolator) {
        mEndInterpolator = endInterpolator;
    }
}
