package net.lucode.hackware.magicindicator.buildins.circlenavigator;

import net.lucode.hackware.magicindicator.abs.IPagerNavigator;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import ohos.agp.animation.Animator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSlider;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * 圆圈式的指示器
 *
 * @since 2020-09-29
 */
public class CircleNavigator extends Component implements IPagerNavigator, Component.TouchEventListener,
        Component.DrawTask, Component.EstimateSizeListener, ComponentContainer.ArrangeListener {
    private int mRadius;
    private int mCircleColor;
    private int mStrokeWidth;
    private int mCircleSpacing; // 间距
    private int mCurrentIndex; // 当前位置
    private int mTotalCount; // 总数
    private int mStartInterpolator = Animator.CurveType.LINEAR;

    private Paint mPaint = new Paint();
    private List<Point> mCirclePoints = new ArrayList<Point>();
    private float mIndicatorX;

    // 事件回调
    private boolean mIsTouchable;
    private OnCircleClickListener mCircleClickListener;
    private float mDownX;
    private float mDownY;
    private int mTouchSlop;

    private boolean mIsFollowTouch = true; // 是否跟随手指滑动

    /**
     * CircleNavigator
     *
     * @param context context
     */
    public CircleNavigator(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        mTouchSlop = 8;
        mRadius = UIUtil.vp2px(context, 3);
        mCircleSpacing = UIUtil.vp2px(context, 8);
        mStrokeWidth = UIUtil.vp2px(context, 1);
        setTouchEventListener(this);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        setEstimatedSize(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
        return false;
    }

    private int measureWidth(int widthMeasureSpec) {
        int mode = EstimateSpec.getMode(widthMeasureSpec);
        int width = EstimateSpec.getSize(widthMeasureSpec);
        int result = 0;
        switch (mode) {
            case EstimateSpec.PRECISE:
                result = width;
                break;
            case EstimateSpec.NOT_EXCEED:
            case EstimateSpec.UNCONSTRAINT:
                result = mTotalCount * mRadius * 2 + (mTotalCount - 1) * mCircleSpacing
                        + getPaddingLeft() + getPaddingRight() + mStrokeWidth * 2;
                break;
            default:
                break;
        }
        return result;
    }

    private int measureHeight(int heightMeasureSpec) {
        int mode = EstimateSpec.getMode(heightMeasureSpec);
        int height = EstimateSpec.getSize(heightMeasureSpec);
        int result = 0;
        switch (mode) {
            case EstimateSpec.PRECISE:
                result = height;
                break;
            case EstimateSpec.NOT_EXCEED:
            case EstimateSpec.UNCONSTRAINT:
                result = mRadius * 2 + mStrokeWidth * 2 + getPaddingTop() + getPaddingBottom();
                break;
            default:
                break;
        }
        return result;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mPaint.setColor(new Color(mCircleColor));
        drawCircles(canvas);
        drawIndicator(canvas);
        prepareCirclePoints();
    }

    private void drawCircles(Canvas canvas) {
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setSubpixelAntiAlias(true);
        mPaint.setStrokeWidth(mStrokeWidth);
        for (int i = 0, j = mCirclePoints.size(); i < j; i++) {
            Point pointF = mCirclePoints.get(i);
            canvas.drawCircle(pointF.getPointX(), (int) (getHeight() / 2.0f + 0.5f), mRadius, mPaint);
        }
    }

    private void drawIndicator(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        if (mCirclePoints.size() > 0) {
            canvas.drawCircle(mIndicatorX, (int) (getHeight() / 2.0f + 0.5f), mRadius, mPaint);
        }
    }

    private void prepareCirclePoints() {
        mCirclePoints.clear();
        if (mTotalCount > 0) {
            int heightY = (int) (getHeight() / 2.0f + 0.5f);
            int centerSpacing = mRadius * 2 + mCircleSpacing;
            int startX = mRadius + (int) (mStrokeWidth / 2.0f + 0.5f) + getPaddingLeft();
            for (int i = 0; i < mTotalCount; i++) {
                Point pointF = new Point(startX, heightY);
                mCirclePoints.add(pointF);
                startX += centerSpacing;
            }
            mIndicatorX = mCirclePoints.get(mCurrentIndex).getPointX();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mIsFollowTouch) {
            if (mCirclePoints.isEmpty()) {
                return;
            }

            int currentPosition = Math.min(mCirclePoints.size() - 1, position);
            int nextPosition;
            if (positionOffsetPixels > 0) {
                nextPosition = Math.min(mCirclePoints.size() - 1, position + 1);
            } else {
                nextPosition = Math.min(mCirclePoints.size() - 1, position - 1);
                if (position == 0) {
                    nextPosition = 1;
                }
            }
            Point current = mCirclePoints.get(currentPosition);
            Point next = mCirclePoints.get(nextPosition);

            mIndicatorX = current.getPointX() + (next.getPointX() - current.getPointX()) * positionOffset;

            addDrawTask(this);
            invalidate();
        }
    }

    @Override
    public void onPageSelected(int position) {
        mCurrentIndex = position;
        if (!mIsFollowTouch) {
            mIndicatorX = mCirclePoints.get(mCurrentIndex).getPointX();
            addDrawTask(this);
            invalidate();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == PageSlider.SLIDING_STATE_IDLE) {
            mIndicatorX = mCirclePoints.get(mCurrentIndex).getPointX();
            addDrawTask(this);
            invalidate();
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        float pointerX = touchEvent.getPointerPosition(touchEvent.getIndex()).getX();
        float pointerY = touchEvent.getPointerPosition(touchEvent.getIndex()).getY();
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                if (mIsTouchable) {
                    mDownX = pointerX;
                    mDownY = pointerY;
                    return true;
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (mCircleClickListener != null) {
                    if (Math.abs(pointerX - mDownX) <= mTouchSlop && Math.abs(pointerY - mDownY) <= mTouchSlop) {
                        float max = Float.MAX_VALUE;
                        int index = 0;
                        for (int i = 0; i < mCirclePoints.size(); i++) {
                            Point pointF = mCirclePoints.get(i);
                            float offset = Math.abs(pointF.getPointX() - pointerX);
                            if (offset < max) {
                                max = offset;
                                index = i;
                            }
                        }
                        mCircleClickListener.onClick(index);
                    }
                }
                break;
            default:
                break;
        }
        return false;
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        prepareCirclePoints();
        return false;
    }

    @Override
    public void onAttachToMagicIndicator() {
    }

    @Override
    public void notifyDataSetChanged() {
        prepareCirclePoints();
        addDrawTask(this);
        invalidate();
    }

    @Override
    public void onDetachFromMagicIndicator() {
    }

    /**
     * getRadius
     *
     * @return mRadius
     */
    public int getRadius() {
        return mRadius;
    }

    /**
     * setRadius
     *
     * @param radius radius
     */
    public void setRadius(int radius) {
        mRadius = radius;
        prepareCirclePoints();
        addDrawTask(this);
    }

    /**
     * getCircleColor
     *
     * @return mCircleColor
     */
    public int getCircleColor() {
        return mCircleColor;
    }

    /**
     * setCircleColor
     *
     * @param circleColor circleColor
     */
    public void setCircleColor(int circleColor) {
        mCircleColor = circleColor;
        prepareCirclePoints();
        addDrawTask(this);
    }

    /**
     * getStrokeWidth
     *
     * @return mStrokeWidth
     */
    public int getStrokeWidth() {
        return mStrokeWidth;
    }

    /**
     * setStrokeWidth
     *
     * @param strokeWidth strokeWidth
     */
    public void setStrokeWidth(int strokeWidth) {
        mStrokeWidth = strokeWidth;
        addDrawTask(this);
    }

    /**
     * getCircleSpacing
     *
     * @return mCircleSpacing
     */
    public int getCircleSpacing() {
        return mCircleSpacing;
    }

    /**
     * setCircleSpacing
     *
     * @param circleSpacing circleSpacing
     */
    public void setCircleSpacing(int circleSpacing) {
        mCircleSpacing = circleSpacing;
        prepareCirclePoints();
        addDrawTask(this);
    }

    /**
     * getStartInterpolator
     *
     * @return mStartInterpolator
     */
    public int getStartInterpolator() {
        return mStartInterpolator;
    }

    /**
     * setStartInterpolator
     *
     * @param startInterpolator startInterpolator
     */
    public void setStartInterpolator(int startInterpolator) {
        mStartInterpolator = startInterpolator;
    }

    /**
     * getCircleCount
     *
     * @return mTotalCount
     */
    public int getCircleCount() {
        return mTotalCount;
    }

    /**
     * setCircleCount
     *
     * @param count count
     */
    public void setCircleCount(int count) {
        mTotalCount = count; // 此处不调用invalidate，让外部调用notifyDataSetChanged
    }

    /**
     * isTouchable
     *
     * @return mIsTouchable
     */
    public boolean isTouchable() {
        return mIsTouchable;
    }

    /**
     * setTouchable
     *
     * @param isTouchable isTouchable
     */
    public void setTouchable(boolean isTouchable) {
        mIsTouchable = isTouchable;
    }

    /**
     * isFollowTouch
     *
     * @return mIsFollowTouch
     */
    public boolean isFollowTouch() {
        return mIsFollowTouch;
    }

    /**
     * setFollowTouch
     *
     * @param isFollowTouch isFollowTouch
     */
    public void setFollowTouch(boolean isFollowTouch) {
        mIsFollowTouch = isFollowTouch;
    }

    /**
     * getCircleClickListener
     *
     * @return mCircleClickListener
     */
    public OnCircleClickListener getCircleClickListener() {
        return mCircleClickListener;
    }

    /**
     * setCircleClickListener
     *
     * @param circleClickListener circleClickListener
     */
    public void setCircleClickListener(OnCircleClickListener circleClickListener) {
        if (!mIsTouchable) {
            mIsTouchable = true;
        }
        mCircleClickListener = circleClickListener;
    }

    /**
     * OnCircleClickListener interface
     *
     * @since 2020-09-29
     */
    public interface OnCircleClickListener {
        /**
         * onClick
         *
         * @param index index
         */
        void onClick(int index);
    }
}
