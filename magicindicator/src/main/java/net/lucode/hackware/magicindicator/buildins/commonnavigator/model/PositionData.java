package net.lucode.hackware.magicindicator.buildins.commonnavigator.model;

/**
 * 保存指示器标题的坐标
 *
 * @since 2020-09-29
 */
public class PositionData {
    /**
     * Left
     */
    public int mLeft;

    /**
     * Top
     */
    public int mTop;

    /**
     * Right
     */
    public int mRight;

    /**
     * Bottom
     */
    public int mBottom;

    /**
     * ContentLeft
     */
    public int mContentLeft;

    /**
     * ContentTop
     */
    public int mContentTop;

    /**
     * ContentRight
     */
    public int mContentRight;

    /**
     * ContentBottom
     */
    public int mContentBottom;

    /**
     * width
     *
     * @return mRight - mLeft
     */
    public int width() {
        return mRight - mLeft;
    }

    /**
     * height
     *
     * @return mBottom - mTop
     */
    public int height() {
        return mBottom - mTop;
    }

    /**
     * contentWidth
     *
     * @return  mContentRight - mContentLeft
     */
    public int contentWidth() {
        return mContentRight - mContentLeft;
    }

    /**
     * contentHeight
     *
     * @return mContentBottom - mContentTop
     */
    public int contentHeight() {
        return mContentBottom - mContentTop;
    }

    /**
     * horizontalCenter
     *
     * @return mLeft + width() / 2
     */
    public int horizontalCenter() {
        return mLeft + width() / 2;
    }

    /**
     * verticalCenter
     *
     * @return mTop + height() / 2
     */
    public int verticalCenter() {
        return mTop + height() / 2;
    }
}
