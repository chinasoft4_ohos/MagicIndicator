package net.lucode.hackware.magicindicator.buildins.commonnavigator.titles;

import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IMeasurablePagerTitleView;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * 带文本的指示器标题
 *
 * @since 2020-09-29
 */
public class SimplePagerTitleView extends Text implements IMeasurablePagerTitleView, Component.DrawTask {
    protected int mSelectedColor;
    protected int mNormalColor;
    private Paint mPaint;
    private boolean mIsShowDividers = false;

    /**
     * SimplePagerTitleView
     *
     * @param context context
     */
    public SimplePagerTitleView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        setTextAlignment(TextAlignment.CENTER);
        int padding = UIUtil.vp2px(context, 10);

        setPadding(padding, 0, padding, 0);
        setTextSize(16, Text.TextSizeType.FP);
        setMultipleLine(false);
        setTruncationMode(TruncationMode.ELLIPSIS_AT_END);
        mPaint = new Paint();
        mPaint.setStrokeWidth(2);
        mPaint.setColor(new Color(Color.getIntColor("#CCCCCC")));
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mIsShowDividers) {
            canvas.drawLine(2, 50, 2, getEstimatedHeight() - 40, mPaint);
        }
    }

    @Override
    public void onSelected(int index, int totalCount) {
        setTextColor(new Color(mSelectedColor));
    }

    @Override
    public void onDeselected(int index, int totalCount) {
        setTextColor(new Color(mNormalColor));
    }

    @Override
    public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
    }

    @Override
    public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
    }

    @Override
    public int getContentLeft() {
        String longestString = "";
        if (getText().toString().contains("\n")) {
            String[] brokenStrings = getText().toString().split("\\n");
            for (String each : brokenStrings) {
                if (each.length() > longestString.length()) {
                    longestString = each;
                }
            }
        } else {
            longestString = getText().toString();
        }

        Rect bound = new Paint().getTextBounds(longestString);
        int contentWidth = bound.getWidth();
        return getLeft() + getWidth() / 2 - contentWidth / 2;
    }

    @Override
    public int getContentTop() {
        Paint.FontMetrics metrics = new Paint().getFontMetrics();
        float contentHeight = metrics.bottom - metrics.top;
        return getHeight() / 2 - (int) contentHeight / 2;
    }

    @Override
    public int getContentRight() {
        String longestString = "";
        if (getText().toString().contains("\n")) {
            String[] brokenStrings = getText().toString().split("\\n");
            for (String each : brokenStrings) {
                if (each.length() > longestString.length()) {
                    longestString = each;
                }
            }
        } else {
            longestString = getText().toString();
        }
        Rect bound = new Paint().getTextBounds(longestString);
        int contentWidth = bound.getWidth();
        return getLeft() + getWidth() / 2 + contentWidth / 2;
    }

    @Override
    public int getContentBottom() {
        Paint.FontMetrics metrics = new Paint().getFontMetrics();
        float contentHeight = metrics.bottom - metrics.top;
        return getHeight() / 2 + (int) contentHeight / 2;
    }

    /**
     * getSelectedColor
     *
     * @return mSelectedColor
     */
    public int getSelectedColor() {
        return mSelectedColor;
    }

    /**
     * setSelectedColor
     *
     * @param selectedColor selectedColor
     */
    public void setSelectedColor(int selectedColor) {
        mSelectedColor = selectedColor;
    }

    /**
     * getNormalColor
     *
     * @return mNormalColor
     */
    public int getNormalColor() {
        return mNormalColor;
    }

    /**
     * setNormalColor
     *
     * @param normalColor normalColor
     */
    public void setNormalColor(int normalColor) {
        mNormalColor = normalColor;
    }

    /**
     * show Line
     *
     * @param isShowDividers isShowDividers
     */
    public void showDividers(boolean isShowDividers) {
        mIsShowDividers = isShowDividers;
    }
}
