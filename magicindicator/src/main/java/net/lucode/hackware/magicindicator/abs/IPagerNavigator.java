package net.lucode.hackware.magicindicator.abs;

/**
 * 抽象的ViewPager导航器
 *
 * @since 2020-09-29
 */
public interface IPagerNavigator {
    /**
     * onPageScrolled 回调
     *
     * @param position position
     * @param positionOffset positionOffset
     * @param positionOffsetPixels positionOffsetPixels
     */
    void onPageScrolled(int position, float positionOffset, int positionOffsetPixels);

    /**
     * onPageSelected 回调
     *
     * @param position position
     */
    void onPageSelected(int position);

    /**
     * onPageScrollStateChanged 回调
     *
     * @param state state
     */
    void onPageScrollStateChanged(int state);

    /**
     * 当IPagerNavigator被添加到MagicIndicator时调用
     */
    void onAttachToMagicIndicator();

    /**
     * 当IPagerNavigator从MagicIndicator上移除时调用
     */
    void onDetachFromMagicIndicator();

    /**
     * ViewPager内容改变时需要先调用此方法，自定义的IPagerNavigator应当遵守此约定
     */
    void notifyDataSetChanged();
}
