package net.lucode.hackware.magicindicator;

/**
 * 自定义滚动状态，消除对ViewPager的依赖
 *
 * @since 2020-09-29
 */
public interface ScrollState {
    /**
     * 0
     */
    int SCROLL_STATE_IDLE = 0;

    /**
     * 1
     */
    int SCROLL_STATE_DRAGGING = 1;

    /**
     * 2
     */
    int SCROLL_STATE_SETTLING = 2;
}
