package net.lucode.hackware.magicindicator.buildins.commonnavigator;

import net.lucode.hackware.magicindicator.NavigatorHelper;
import net.lucode.hackware.magicindicator.ResourceTable;
import net.lucode.hackware.magicindicator.ScrollState;
import net.lucode.hackware.magicindicator.abs.IPagerNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IMeasurablePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.model.PositionData;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ScrollView;
import ohos.agp.components.StackLayout;
import ohos.agp.database.DataSetSubscriber;
import ohos.agp.render.Canvas;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * 通用的ViewPager指示器
 *
 * @since 2020-09-29
 */
public class CommonNavigator extends StackLayout implements IPagerNavigator,
        NavigatorHelper.OnNavigatorScrollListener, Component.DrawTask {
    private ScrollView mScrollView;
    private DirectionalLayout mTitleContainer;
    private DirectionalLayout mIndicatorContainer;
    private IPagerIndicator mIndicator;

    private CommonNavigatorAdapter mAdapter;
    private NavigatorHelper mNavigatorHelper;

    /**
     * 提供给外部的参数配置
     */
    private boolean mIsAdjustMode; // 自适应模式，适用于数目固定的、少量的title
    private boolean mIsEnablePivotScroll; // 启动中心点滚动
    private float mScrollPivotX = 0.5f; // 滚动中心点 0.0f - 1.0f
    private boolean mIsSmoothScroll = true; // 是否平滑滚动，适用于 !mAdjustMode && !mFollowTouch
    private boolean mIsFollowTouch = true; // 是否手指跟随滚动
    private int mRightPadding;
    private int mLeftPadding;
    private boolean mIsIndicatorOnTop; // 指示器是否在title上层，默认为下层
    private boolean mIsSkimOver; // 跨多页切换时，中间页是否显示 "掠过" 效果
    private boolean mIsReselectWhenLayout = true; // PositionData准备好时，是否重新选中当前页，为true可保证在极端情况下指示器状态正确

    // 保存每个title的位置信息，为扩展indicator提供保障
    private List<PositionData> mPositionDataList = new ArrayList<PositionData>();

    private DataSetSubscriber mObserver = new DataSetSubscriber() {
        @Override
        public void onChanged() {
            mNavigatorHelper.setTotalCount(mAdapter.getCount()); // 如果使用helper，应始终保证helper中的totalCount为最新
            init();
        }

        @Override
        public void onInvalidated() {
        }
    };

    /**
     * CommonNavigator
     *
     * @param context context
     */
    public CommonNavigator(Context context) {
        super(context);
        mNavigatorHelper = new NavigatorHelper();
        mNavigatorHelper.setNavigatorScrollListener(this);
    }

    @Override
    public void notifyDataSetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * isAdjustMode
     *
     * @return mIsAdjustMode
     */
    public boolean isAdjustMode() {
        return mIsAdjustMode;
    }

    /**
     * setAdjustMode
     *
     * @param isAdjustMode isAdjustMode
     */
    public void setAdjustMode(boolean isAdjustMode) {
        mIsAdjustMode = isAdjustMode;
    }

    /**
     * CommonNavigatorAdapter
     *
     * @return mAdapter
     */
    public CommonNavigatorAdapter getAdapter() {
        return mAdapter;
    }

    /**
     * setAdapter
     *
     * @param adapter adapter
     */
    public void setAdapter(CommonNavigatorAdapter adapter) {
        if (mAdapter == adapter) {
            return;
        }
        if (mAdapter != null) {
            mAdapter.unregisterDataSetObserver(mObserver);
            mAdapter.unregisterDataSetObserver(mObserver);
        }
        mAdapter = adapter;
        if (mAdapter != null) {
            mAdapter.registerDataSetObserver(mObserver);
            mNavigatorHelper.setTotalCount(mAdapter.getCount());
            if (mTitleContainer != null) { // adapter改变时，应该重新init，但是第一次设置adapter不用，onAttachToMagicIndicator中有init
                mAdapter.notifyDataSetChanged();
            }
        } else {
            mNavigatorHelper.setTotalCount(0);
            init();
        }
    }

    private void init() {
        removeAllComponents();
        Component root;
        if (mIsAdjustMode) {
            root = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_pager_navigator_layout_no_scroll, this, false);
        } else {
            root = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_pager_navigator_layout, this, false);
        }

        mScrollView = (ScrollView) root.findComponentById(ResourceTable.Id_scroll_view);

        mTitleContainer = (DirectionalLayout) root.findComponentById(ResourceTable.Id_title_container);
        mTitleContainer.setPadding(mLeftPadding, 0, mRightPadding, 0);

        mIndicatorContainer = (DirectionalLayout) root.findComponentById(ResourceTable.Id_indicator_container);
        if (mIsIndicatorOnTop) {
            mIndicatorContainer.getComponentParent().moveChildToFront(mIndicatorContainer);
        }
        initTitlesAndIndicator();
        addComponent(root);
        addDrawTask(this);
    }

    /**
     * 初始化title和indicator
     */
    private void initTitlesAndIndicator() {
        for (int i = 0, j = mNavigatorHelper.getTotalCount(); i < j; i++) {
            IPagerTitleView view = mAdapter.getTitleView(getContext(), i);
            if (view instanceof Component) {
                Component component = (Component) view;
                DirectionalLayout.LayoutConfig lp;
                if (mIsAdjustMode) {
                    lp = new DirectionalLayout.LayoutConfig(0, LayoutConfig.MATCH_PARENT);
                    lp.weight = mAdapter.getTitleWeight(getContext(), i);
                } else {
                    lp = new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_PARENT);
                }
                mTitleContainer.addComponent(component, lp);
            }
        }
        if (mAdapter != null) {
            mIndicator = mAdapter.getIndicator(getContext());
            if (mIndicator instanceof Component) {
                ComponentContainer.LayoutConfig lp = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_PARENT);
                mIndicatorContainer.addComponent((Component) mIndicator, lp);
            }
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mAdapter != null) {
            if (mIndicator != null) {
                mIndicator.onPositionDataProvide(mPositionDataList);
            }
            if (mIsReselectWhenLayout && mNavigatorHelper.getScrollState() == ScrollState.SCROLL_STATE_IDLE) {
                onPageSelected(mNavigatorHelper.getCurrentIndex());
                onPageScrolled(mNavigatorHelper.getCurrentIndex(), 0.0f, 0);
            }
        }
    }

    /**
     * 获取title的位置信息，为打造不同的指示器、各种效果提供可能
     */
    private void preparePositionData() {
        mPositionDataList.clear();
        for (int i = 0, j = mNavigatorHelper.getTotalCount(); i < j; i++) {
            PositionData data = new PositionData();
            Component component = mTitleContainer.getComponentAt(i);
            if (component != null) {
                component.getWidth();
                data.mLeft = component.getLeft();
                data.mTop = component.getTop();
                data.mRight = component.getRight();
                data.mBottom = component.getBottom();
                if (component instanceof IMeasurablePagerTitleView) {
                    IMeasurablePagerTitleView view = (IMeasurablePagerTitleView) component;
                    data.mContentLeft = view.getContentLeft();
                    data.mContentTop = view.getContentTop();
                    data.mContentRight = view.getContentRight();
                    data.mContentBottom = view.getContentBottom();
                } else {
                    data.mContentLeft = data.mLeft;
                    data.mContentTop = data.mTop;
                    data.mContentRight = data.mRight;
                    data.mContentBottom = data.mBottom;
                }
            }
            mPositionDataList.add(data);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mAdapter != null) {
            mNavigatorHelper.onPageScrolled(position, positionOffset, positionOffsetPixels);
            if (mIndicator != null) {
                mIndicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            // 手指跟随滚动
            if (mScrollView != null && mPositionDataList.size() > 0
                    && position >= 0 && position < mPositionDataList.size()) {
                if (mIsFollowTouch) {
                    int minNextPosition = position + 1;
                    if (positionOffsetPixels < 0) {
                        minNextPosition = position - 1;
                        if (position == 0) {
                            minNextPosition = 0;
                        }
                    }
                    int currentPosition = Math.min(mPositionDataList.size() - 1, position);
                    int nextPosition = Math.min(mPositionDataList.size() - 1, minNextPosition);
                    PositionData current = mPositionDataList.get(currentPosition);
                    PositionData next = mPositionDataList.get(nextPosition);
                    float scrollTo = current.horizontalCenter() - mScrollView.getWidth() * mScrollPivotX;
                    float nextScrollTo = next.horizontalCenter() - mScrollView.getWidth() * mScrollPivotX;
                    mScrollView.scrollTo((int) (scrollTo + (nextScrollTo - scrollTo) * positionOffset), 0);
                }
            }
        }
    }

    /**
     * getScrollPivotX
     *
     * @return mScrollPivotX
     */
    public float getScrollPivotX() {
        return mScrollPivotX;
    }

    /**
     * setScrollPivotX
     *
     * @param scrollPivotX scrollPivotX
     */
    public void setScrollPivotX(float scrollPivotX) {
        mScrollPivotX = scrollPivotX;
    }

    @Override
    public void onPageSelected(int position) {
        if (mAdapter != null) {
            preparePositionData();
            mNavigatorHelper.onPageSelected(position);
            if (mIndicator != null) {
                mIndicator.onPageSelected(position);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (mAdapter != null) {
            mNavigatorHelper.onPageScrollStateChanged(state);
            if (mIndicator != null) {
                mIndicator.onPageScrollStateChanged(state);
            }
        }
    }

    @Override
    public void onAttachToMagicIndicator() {
        init(); // 将初始化延迟到这里
    }

    @Override
    public void onDetachFromMagicIndicator() {
    }

    /**
     * getPagerIndicator
     *
     * @return mIndicator
     */
    public IPagerIndicator getPagerIndicator() {
        return mIndicator;
    }

    /**
     * isEnablePivotScroll
     *
     * @return mIsEnablePivotScroll
     */
    public boolean isEnablePivotScroll() {
        return mIsEnablePivotScroll;
    }

    /**
     * setEnablePivotScroll
     *
     * @param isEnable isEnable
     */
    public void setEnablePivotScroll(boolean isEnable) {
        mIsEnablePivotScroll = isEnable;
    }

    @Override
    public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
        if (mTitleContainer == null) {
            return;
        }
        Component component = mTitleContainer.getComponentAt(index);
        if (component instanceof IPagerTitleView) {
            ((IPagerTitleView) component).onEnter(index, totalCount, enterPercent, isLeftToRight);
        }
    }

    @Override
    public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
        if (mTitleContainer == null) {
            return;
        }
        Component component = mTitleContainer.getComponentAt(index);
        if (component instanceof IPagerTitleView) {
            ((IPagerTitleView) component).onLeave(index, totalCount, leavePercent, isLeftToRight);
        }
    }

    /**
     * isSmoothScroll
     *
     * @return mIsSmoothScroll
     */
    public boolean isSmoothScroll() {
        return mIsSmoothScroll;
    }

    /**
     * setSmoothScroll
     *
     * @param isSmoothScroll isSmoothScroll
     */
    public void setSmoothScroll(boolean isSmoothScroll) {
        mIsSmoothScroll = isSmoothScroll;
    }

    /**
     * isFollowTouch
     *
     * @return mIsFollowTouch
     */
    public boolean isFollowTouch() {
        return mIsFollowTouch;
    }

    /**
     * setFollowTouch
     *
     * @param isFollowTouch isFollowTouch
     */
    public void setFollowTouch(boolean isFollowTouch) {
        mIsFollowTouch = isFollowTouch;
    }

    /**
     * isSkimOver
     *
     * @return mIsSkimOver
     */
    public boolean isSkimOver() {
        return mIsSkimOver;
    }

    /**
     * setSkimOver
     *
     * @param isSkimOver isSkimOver
     */
    public void setSkimOver(boolean isSkimOver) {
        mIsSkimOver = isSkimOver;
        mNavigatorHelper.setSkimOver(isSkimOver);
    }

    @Override
    public void onSelected(int index, int totalCount) {
        if (mTitleContainer == null) {
            return;
        }
        Component component = mTitleContainer.getComponentAt(index);
        if (component instanceof IPagerTitleView) {
            ((IPagerTitleView) component).onSelected(index, totalCount);
        }
        if (!mIsAdjustMode && !mIsFollowTouch && mScrollView != null && mPositionDataList.size() > 0) {
            int currentIndex = Math.min(mPositionDataList.size() - 1, index);
            PositionData current = mPositionDataList.get(currentIndex);
            if (mIsEnablePivotScroll) {
                float scrollTo = current.horizontalCenter() - mScrollView.getWidth() * mScrollPivotX;
                if (mIsSmoothScroll) {
                    mScrollView.fluentScrollTo((int) (scrollTo), 0);
                } else {
                    mScrollView.scrollTo((int) (scrollTo), 0);
                }
            } else {
                // 如果当前项被部分遮挡，则滚动显示完全
                if (mScrollView.getPivotX() + getWidth() > current.mLeft + mLeftPadding) {
                    if (mIsSmoothScroll) {
                        mScrollView.fluentScrollTo(current.mLeft, 0);
                    } else {
                        mScrollView.scrollTo(current.mLeft, 0);
                    }
                } else if (mScrollView.getPivotX() + getWidth() < current.mRight - mRightPadding) {
                    if (mIsSmoothScroll) {
                        mScrollView.fluentScrollTo(current.mRight - getWidth() / 2, 0);
                    } else {
                        mScrollView.scrollTo(current.mRight - getWidth(), 0);
                    }
                }
            }
        }
    }

    @Override
    public void onDeselected(int index, int totalCount) {
        if (mTitleContainer == null) {
            return;
        }
        Component component = mTitleContainer.getComponentAt(index);
        if (component instanceof IPagerTitleView) {
            ((IPagerTitleView) component).onDeselected(index, totalCount);
        }
    }

    /**
     * getPagerTitleView
     *
     * @param index index
     * @return IPagerTitleView
     */
    public IPagerTitleView getPagerTitleView(int index) {
        if (mTitleContainer == null) {
            return null;
        }
        return (IPagerTitleView) mTitleContainer.getComponentAt(index);
    }

    /**
     * getTitleContainer
     *
     * @return mTitleContainer
     */
    public DirectionalLayout getTitleContainer() {
        return mTitleContainer;
    }

    /**
     * getRightPadding
     *
     * @return mRightPadding
     */
    public int getRightPadding() {
        return mRightPadding;
    }

    /**
     * setRightPadding
     *
     * @param rightPadding rightPadding
     */
    public void setRightPadding(int rightPadding) {
        mRightPadding = rightPadding;
    }

    /**
     * getLeftPadding
     *
     * @return mLeftPadding
     */
    public int getLeftPadding() {
        return mLeftPadding;
    }

    /**
     * setLeftPadding
     *
     * @param leftPadding leftPadding
     */
    public void setLeftPadding(int leftPadding) {
        mLeftPadding = leftPadding;
    }

    /**
     * isIndicatorOnTop
     *
     * @return mIsIndicatorOnTop
     */
    public boolean isIndicatorOnTop() {
        return mIsIndicatorOnTop;
    }

    /**
     * setIndicatorOnTop
     *
     * @param isIndicatorOnTop isIndicatorOnTop
     */
    public void setIndicatorOnTop(boolean isIndicatorOnTop) {
        mIsIndicatorOnTop = isIndicatorOnTop;
    }

    /**
     * isReselectWhenLayout
     *
     * @return mIsReselectWhenLayout
     */
    public boolean isReselectWhenLayout() {
        return mIsReselectWhenLayout;
    }

    /**
     * setReselectWhenLayout
     *
     * @param isReselectWhenLayout isReselectWhenLayout
     */
    public void setReselectWhenLayout(boolean isReselectWhenLayout) {
        mIsReselectWhenLayout = isReselectWhenLayout;
    }
}
