package net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge;

/**
 * 角标的定位规则
 *
 * @since 2020-09-29
 */
public class BadgeRule {
    private BadgeAnchor mAnchor;
    private int mOffset;

    /**
     * BadgeRule
     *
     * @param anchor anchor
     * @param offset offset
     */
    public BadgeRule(BadgeAnchor anchor, int offset) {
        mAnchor = anchor;
        mOffset = offset;
    }

    /**
     * getAnchor
     *
     * @return mAnchor
     */
    public BadgeAnchor getAnchor() {
        return mAnchor;
    }

    /**
     * setAnchor
     *
     * @param anchor anchor
     */
    public void setAnchor(BadgeAnchor anchor) {
        mAnchor = anchor;
    }

    /**
     * getOffset
     *
     * @return mOffset
     */
    public int getOffset() {
        return mOffset;
    }

    /**
     * setOffset
     *
     * @param offset offset
     */
    public void setOffset(int offset) {
        mOffset = offset;
    }
}
