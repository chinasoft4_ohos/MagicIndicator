package net.lucode.hackware.magicindicator.buildins.commonnavigator.abs;

import ohos.agp.database.DataSetPublisher;
import ohos.agp.database.DataSetSubscriber;
import ohos.app.Context;

/**
 * CommonNavigator适配器，通过它可轻松切换不同的指示器样式
 *
 * @since 2020-09-29
 */
public abstract class CommonNavigatorAdapter {
    private final DataSetPublisher mDataSetObservable = new DataSetPublisher();

    /**
     * getCount
     *
     * @return Count
     */
    public abstract int getCount();

    /**
     * getTitleView
     *
     * @param context context
     * @param index index
     * @return IPagerTitleView
     */
    public abstract IPagerTitleView getTitleView(Context context, int index);

    /**
     * getIndicator
     *
     * @param context context
     * @return IPagerIndicator
     */
    public abstract IPagerIndicator getIndicator(Context context);

    /**
     * getTitleWeight
     *
     * @param context context
     * @param index index
     * @return 1
     */
    public float getTitleWeight(Context context, int index) {
        return 1;
    }

    /**
     * registerDataSetObserver
     *
     * @param observer observer
     */
    public final void registerDataSetObserver(DataSetSubscriber observer) {
        mDataSetObservable.registerSubscriber(observer);
    }

    /**
     * unregisterDataSetObserver
     *
     * @param observer observer
     */
    public final void unregisterDataSetObserver(DataSetSubscriber observer) {
        mDataSetObservable.unregisterSubscriber(observer);
    }

    /**
     * notifyDataSetChanged
     */
    public final void notifyDataSetChanged() {
        mDataSetObservable.notifyChanged();
    }

    /**
     * notifyDataSetInvalidated
     */
    public final void notifyDataSetInvalidated() {
        mDataSetObservable.informInvalidated();
    }
}
