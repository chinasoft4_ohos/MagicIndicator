package net.lucode.hackware.magicindicator.buildins.commonnavigator.titles;

import net.lucode.hackware.magicindicator.buildins.ArgbEvaluatorHolder;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * 两种颜色过渡的指示器标题
 *
 * @since 2020-09-29
 */
public class ColorTransitionPagerTitleView extends SimplePagerTitleView {
    /**
     * ColorTransitionPagerTitleView
     *
     * @param context context
     */
    public ColorTransitionPagerTitleView(Context context) {
        super(context);
    }

    @Override
    public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
        int color = ArgbEvaluatorHolder.eval(leavePercent, mSelectedColor, mNormalColor);
        setTextColor(new Color(color));
    }

    @Override
    public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
        int color = ArgbEvaluatorHolder.eval(enterPercent, mNormalColor, mSelectedColor);
        setTextColor(new Color(color));
    }

    @Override
    public void onSelected(int index, int totalCount) {
    }

    @Override
    public void onDeselected(int index, int totalCount) {
    }
}
