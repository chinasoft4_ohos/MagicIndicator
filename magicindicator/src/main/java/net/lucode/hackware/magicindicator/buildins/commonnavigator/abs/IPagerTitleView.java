package net.lucode.hackware.magicindicator.buildins.commonnavigator.abs;

/**
 * 抽象的指示器标题，适用于CommonNavigator
 *
 * @since 2020-09-29
 */
public interface IPagerTitleView {
    /**
     * onSelected
     *
     * @param index index
     * @param totalCount totalCount 被选中
     */
    void onSelected(int index, int totalCount);

    /**
     * onDeselected
     *
     * @param index index
     * @param totalCount totalCount 未被选中
     */
    void onDeselected(int index, int totalCount);

    /**
     * 离开
     *
     * @param index index
     * @param totalCount totalCount
     * @param leavePercent 离开的百分比, 0.0f - 1.0f
     * @param isLeftToRight 从左至右离开
     */
    void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight);

    /**
     * 进入
     *
     * @param index index
     * @param totalCount totalCount
     * @param enterPercent 进入的百分比, 0.0f - 1.0f
     * @param isLeftToRight 从左至右离开
     */
    void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight);
}
