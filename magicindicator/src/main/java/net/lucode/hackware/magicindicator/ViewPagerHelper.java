package net.lucode.hackware.magicindicator;

import ohos.agp.components.PageSlider;

/**
 * 简化和ViewPager绑定
 *
 * @since 2020-09-29
 */
public class ViewPagerHelper {
    private ViewPagerHelper() {
    }

    /**
     * bind
     *
     * @param magicIndicator magicIndicator
     * @param viewPager viewPager
     */
    public static void bind(final MagicIndicator magicIndicator, PageSlider viewPager) {
        viewPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            int mPosition = 0;
            @Override
            public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
                System.out.println(position + "====position========stt=====onPageSliding=======mPosition==" + mPosition);
                if (Math.abs(mPosition - position) == 1 || Math.abs(mPosition - position) == 0) {
                    magicIndicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
                } else {
                    if (positionOffsetPixels > 0) {
                        magicIndicator.onPageScrolled(mPosition - 1, positionOffset, positionOffsetPixels);
                    } else {
                        magicIndicator.onPageScrolled(mPosition + 1, positionOffset, positionOffsetPixels);
                    }
                }
            }

            @Override
            public void onPageSlideStateChanged(int state) {
                magicIndicator.onPageScrollStateChanged(state);
            }

            @Override
            public void onPageChosen(int itemPos) {
                mPosition = itemPos;
                System.out.println(itemPos + "====itemPos====stt==========");
                magicIndicator.onPageSelected(itemPos);
            }
        });
    }
}
