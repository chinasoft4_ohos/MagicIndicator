package net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge;

/**
 * 角标的锚点
 *
 * @since 2020-09-29
 */
public enum BadgeAnchor {
    /**
     * left
     */
    LEFT,
    /**
     * top
     */
    TOP,
    /**
     * right
     */
    RIGHT,
    /**
     * bottom
     */
    BOTTOM,
    /**
     * content left
     */
    CONTENT_LEFT,
    /**
     * content top
     */
    CONTENT_TOP,
    /**
     * content right
     */
    CONTENT_RIGHT,
    /**
     * content bottom
     */
    CONTENT_BOTTOM,
    /**
     * center x
     */
    CENTER_X,
    /**
     * center y
     */
    CENTER_Y,
    /**
     * left edge center x
     */
    LEFT_EDGE_CENTER_X,
    /**
     * top edge center y
     */
    TOP_EDGE_CENTER_Y,
    /**
     * right edge center x
     */
    RIGHT_EDGE_CENTER_X,
    /**
     * bottom edge center y
     */
    BOTTOM_EDGE_CENTER_Y
}
