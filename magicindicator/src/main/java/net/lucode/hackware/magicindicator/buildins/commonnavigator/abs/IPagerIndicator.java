package net.lucode.hackware.magicindicator.buildins.commonnavigator.abs;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.model.PositionData;

import java.util.List;

/**
 * 抽象的viewpager指示器，适用于CommonNavigator
 *
 * @since 2020-09-29
 */
public interface IPagerIndicator {
    /**
     * onPageScrolled
     *
     * @param position position
     * @param positionOffset positionOffset
     * @param positionOffsetPixels positionOffsetPixels
     */
    void onPageScrolled(int position, float positionOffset, int positionOffsetPixels);

    /**
     * onPageSelected
     *
     * @param position position
     */
    void onPageSelected(int position);

    /**
     * onPageScrollStateChanged
     *
     * @param state state
     */
    void onPageScrollStateChanged(int state);

    /**
     * onPositionDataProvide
     *
     * @param dataList dataList
     */
    void onPositionDataProvide(List<PositionData> dataList);
}
