package net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IMeasurablePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.render.Canvas;
import ohos.app.Context;

/**
 * 支持显示角标的title，角标布局可自定义
 *
 * @since 2020-09-29
 */
public class BadgePagerTitleView extends StackLayout implements IMeasurablePagerTitleView,
        ComponentContainer.ArrangeListener, Component.DrawTask {
    private IPagerTitleView mInnerPagerTitleView;
    private Component mBadgeView;
    private boolean mIsAutoCancelBadge = true;

    private BadgeRule mXBadgeRule;
    private BadgeRule mYBadgeRule;

    /**
     * BadgePagerTitleView
     *
     * @param context context
     */
    public BadgePagerTitleView(Context context) {
        super(context);
        setArrangeListener(this);
        addDrawTask(this);
    }

    @Override
    public void onSelected(int index, int totalCount) {
        if (mInnerPagerTitleView != null) {
            mInnerPagerTitleView.onSelected(index, totalCount);
        }
        if (mIsAutoCancelBadge) {
            setBadgeView(null);
        }
    }

    @Override
    public void onDeselected(int index, int totalCount) {
        if (mInnerPagerTitleView != null) {
            mInnerPagerTitleView.onDeselected(index, totalCount);
        }
    }

    @Override
    public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
        if (mInnerPagerTitleView != null) {
            mInnerPagerTitleView.onLeave(index, totalCount, leavePercent, isLeftToRight);
        }
    }

    @Override
    public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
        if (mInnerPagerTitleView != null) {
            mInnerPagerTitleView.onEnter(index, totalCount, enterPercent, isLeftToRight);
        }
    }

    /**
     * getInnerPagerTitleView
     *
     * @return mInnerPagerTitleView
     */
    public IPagerTitleView getInnerPagerTitleView() {
        return mInnerPagerTitleView;
    }

    /**
     * setInnerPagerTitleView
     *
     * @param innerPagerTitleView innerPagerTitleView
     */
    public void setInnerPagerTitleView(IPagerTitleView innerPagerTitleView) {
        if (mInnerPagerTitleView == innerPagerTitleView) {
            return;
        }
        mInnerPagerTitleView = innerPagerTitleView;
        removeAllComponents();
        if (mInnerPagerTitleView instanceof Component) {
            StackLayout.LayoutConfig lp = new StackLayout.LayoutConfig(LayoutConfig.MATCH_PARENT,
                    LayoutConfig.MATCH_PARENT);
            addComponent((Component) mInnerPagerTitleView, lp);
        }
        if (mBadgeView != null) {
            addComponent(mBadgeView);
        }
    }

    public Component getBadgeView() {
        return mBadgeView;
    }

    /**
     * setBadgeView
     *
     * @param badgeView badgeView
     */
    public void setBadgeView(Component badgeView) {
        if (mBadgeView == badgeView) {
            return;
        }
        mBadgeView = badgeView;
        removeAllComponents();
        if (mInnerPagerTitleView instanceof Component) {
            StackLayout.LayoutConfig lp = new StackLayout.LayoutConfig(LayoutConfig.MATCH_PARENT,
                    LayoutConfig.MATCH_PARENT);
            addComponent((Component) mInnerPagerTitleView, lp);
        }
        if (mBadgeView != null) {
            addComponent(mBadgeView);
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mInnerPagerTitleView instanceof Component && mBadgeView != null) {
            int[] position = new int[14]; // 14种角标定位方式
            Component titleView = (Component) mInnerPagerTitleView;
            position[0] = titleView.getLeft();
            position[1] = titleView.getTop();
            position[2] = titleView.getRight();
            position[3] = titleView.getBottom();
            if (mInnerPagerTitleView instanceof IMeasurablePagerTitleView) {
                IMeasurablePagerTitleView view = (IMeasurablePagerTitleView) mInnerPagerTitleView;
                position[4] = view.getContentLeft();
                position[5] = view.getContentTop();
                position[6] = view.getContentRight();
                position[7] = view.getContentBottom();
            } else {
                for (int i = 4; i < 8; i++) {
                    position[i] = position[i - 4];
                }
            }
            position[8] = titleView.getWidth() / 2;
            position[9] = titleView.getHeight() / 2;
            position[10] = position[4] / 2;
            position[11] = position[5] / 2;
            position[12] = position[6] + (position[2] - position[6]) / 2;
            position[13] = position[7] + (position[3] - position[7]) / 2;

            // 根据设置的BadgeRule调整角标的位置
            if (mXBadgeRule != null) {
                int viewX = position[mXBadgeRule.getAnchor().ordinal()];
                int offset = mXBadgeRule.getOffset();
                int newLeft = viewX + offset;
                mBadgeView.setContentPositionX(newLeft - mBadgeView.getLeft());
            }
            if (mYBadgeRule != null) {
                int viewY = position[mYBadgeRule.getAnchor().ordinal()];
                int offset = mYBadgeRule.getOffset();
                int newTop = viewY + offset;
                mBadgeView.setTranslationY(newTop - mBadgeView.getLeft());
            }
        }
    }

    @Override
    public boolean onArrange(int left, int top, int right, int bottom) {
        return false;
    }

    @Override
    public int getContentLeft() {
        if (mInnerPagerTitleView instanceof IMeasurablePagerTitleView) {
            return getLeft() + ((IMeasurablePagerTitleView) mInnerPagerTitleView).getContentLeft();
        }
        return getLeft();
    }

    @Override
    public int getContentTop() {
        if (mInnerPagerTitleView instanceof IMeasurablePagerTitleView) {
            return ((IMeasurablePagerTitleView) mInnerPagerTitleView).getContentTop();
        }
        return getTop();
    }

    @Override
    public int getContentRight() {
        if (mInnerPagerTitleView instanceof IMeasurablePagerTitleView) {
            return getLeft() + ((IMeasurablePagerTitleView) mInnerPagerTitleView).getContentRight();
        }
        return getRight();
    }

    @Override
    public int getContentBottom() {
        if (mInnerPagerTitleView instanceof IMeasurablePagerTitleView) {
            return ((IMeasurablePagerTitleView) mInnerPagerTitleView).getContentBottom();
        }
        return getBottom();
    }

    public BadgeRule getXBadgeRule() {
        return mXBadgeRule;
    }

    /**
     * setXBadgeRule
     *
     * @param badgeRule badgeRule
     */
    public void setXBadgeRule(BadgeRule badgeRule) {
        if (badgeRule != null) {
            BadgeAnchor anchor = badgeRule.getAnchor();
            if (anchor != BadgeAnchor.LEFT && anchor != BadgeAnchor.RIGHT && anchor != BadgeAnchor.CONTENT_LEFT
                    && anchor != BadgeAnchor.CONTENT_RIGHT && anchor != BadgeAnchor.CENTER_X
                    && anchor != BadgeAnchor.LEFT_EDGE_CENTER_X && anchor != BadgeAnchor.RIGHT_EDGE_CENTER_X) {
                throw new IllegalArgumentException("x badge rule is wrong.");
            }
        }
        mXBadgeRule = badgeRule;
    }

    /**
     * getYBadgeRule
     *
     * @return mYBadgeRule
     */
    public BadgeRule getYBadgeRule() {
        return mYBadgeRule;
    }

    /**
     * setYBadgeRule
     *
     * @param badgeRule badgeRule
     */
    public void setYBadgeRule(BadgeRule badgeRule) {
        if (badgeRule != null) {
            BadgeAnchor anchor = badgeRule.getAnchor();
            if (anchor != BadgeAnchor.TOP && anchor != BadgeAnchor.BOTTOM && anchor != BadgeAnchor.CONTENT_TOP
                    && anchor != BadgeAnchor.CONTENT_BOTTOM && anchor != BadgeAnchor.CENTER_Y
                    && anchor != BadgeAnchor.TOP_EDGE_CENTER_Y && anchor != BadgeAnchor.BOTTOM_EDGE_CENTER_Y) {
                throw new IllegalArgumentException("y badge rule is wrong.");
            }
        }
        mYBadgeRule = badgeRule;
    }

    /**
     * isAutoCancelBadge
     *
     * @return mAutoCancelBadge
     */
    public boolean isAutoCancelBadge() {
        return mIsAutoCancelBadge;
    }

    /**
     * setAutoCancelBadge
     *
     * @param isAutoCancelBadge isAutoCancelBadge
     */
    public void setAutoCancelBadge(boolean isAutoCancelBadge) {
        mIsAutoCancelBadge = isAutoCancelBadge;
    }
}
