package net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators;

import net.lucode.hackware.magicindicator.FragmentContainerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.model.PositionData;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.List;

/**
 * 用于测试的指示器，可用来检测自定义的IMeasurablePagerTitleView是否正确测量内容区域
 *
 * @since 2020-09-29
 */
public class TestPagerIndicator extends Component implements IPagerIndicator, Component.DrawTask {
    private Paint mPaint;
    private Color mOutRectColor;
    private Color mInnerRectColor;
    private RectFloat mOutRect = new RectFloat();
    private RectFloat mInnerRect = new RectFloat();

    private List<PositionData> mPositionDataList;

    /**
     * TestPagerIndicator
     *
     * @param context context
     */
    public TestPagerIndicator(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        mPaint = new Paint();
        mPaint.setSubpixelAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mOutRectColor = Color.RED;
        mInnerRectColor = Color.GREEN;
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mPaint.setColor(mOutRectColor);
        canvas.drawRect(mOutRect, mPaint);
        mPaint.setColor(mInnerRectColor);
        canvas.drawRect(mInnerRect, mPaint);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mPositionDataList == null || mPositionDataList.isEmpty()) {
            return;
        }
        int nextPosition = position + 1;
        if (positionOffsetPixels < 0) {
            nextPosition = position - 1;
        }

        // 计算锚点位置
        PositionData current = FragmentContainerHelper.getImitativePositionData(mPositionDataList, position);
        PositionData next = FragmentContainerHelper.getImitativePositionData(mPositionDataList, nextPosition);

        mOutRect.left = current.mLeft + (next.mLeft - current.mLeft) * positionOffset;
        mOutRect.top = current.mTop + (next.mTop - current.mTop) * positionOffset;
        mOutRect.right = current.mRight + (next.mRight - current.mRight) * positionOffset;
        mOutRect.bottom = current.mBottom + (next.mBottom - current.mBottom) * positionOffset;

        mInnerRect.left = current.mContentLeft + (next.mContentLeft - current.mContentLeft) * positionOffset;
        mInnerRect.top = current.mContentTop + (next.mContentTop - current.mContentTop) * positionOffset;
        mInnerRect.right = current.mContentRight + (next.mContentRight - current.mContentRight) * positionOffset;
        mInnerRect.bottom = current.mContentBottom + (next.mContentBottom - current.mContentBottom) * positionOffset;

        invalidate();
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPositionDataProvide(List<PositionData> dataList) {
        mPositionDataList = dataList;
    }

    /**
     * getOutRectColor
     *
     * @return mOutRectColor Value
     */
    public int getOutRectColor() {
        return mOutRectColor.getValue();
    }

    /**
     * setOutRectColor
     *
     * @param outRectColor outRectColor
     */
    public void setOutRectColor(int outRectColor) {
        mOutRectColor = new Color(outRectColor);
    }

    /**
     * getInnerRectColor
     *
     * @return mInnerRectColor Value
     */
    public int getInnerRectColor() {
        return mInnerRectColor.getValue();
    }

    /**
     * setInnerRectColor
     *
     * @param innerRectColor innerRectColor
     */
    public void setInnerRectColor(int innerRectColor) {
        mInnerRectColor = new Color(innerRectColor);
    }
}
