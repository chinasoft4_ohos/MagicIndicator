package net.lucode.hackware.magicindicator.buildins.commonnavigator.titles;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IMeasurablePagerTitleView;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * 通用的指示器标题，子元素内容由外部提供，事件回传给外部
 *
 * @since 2020-09-29
 */
public class CommonPagerTitleView extends StackLayout implements IMeasurablePagerTitleView {
    private OnPagerTitleChangeListener mOnPagerTitleChangeListener;
    private ContentPositionDataProvider mContentPositionDataProvider;

    /**
     * CommonPagerTitleView
     *
     * @param context context
     */
    public CommonPagerTitleView(Context context) {
        super(context);
    }

    @Override
    public void onSelected(int index, int totalCount) {
        if (mOnPagerTitleChangeListener != null) {
            mOnPagerTitleChangeListener.onSelected(index, totalCount);
        }
    }

    @Override
    public void onDeselected(int index, int totalCount) {
        if (mOnPagerTitleChangeListener != null) {
            mOnPagerTitleChangeListener.onDeselected(index, totalCount);
        }
    }

    @Override
    public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
        if (mOnPagerTitleChangeListener != null) {
            mOnPagerTitleChangeListener.onLeave(index, totalCount, leavePercent, isLeftToRight);
        }
    }

    @Override
    public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
        if (mOnPagerTitleChangeListener != null) {
            mOnPagerTitleChangeListener.onEnter(index, totalCount, enterPercent, isLeftToRight);
        }
    }

    @Override
    public int getContentLeft() {
        if (mContentPositionDataProvider != null) {
            return mContentPositionDataProvider.getContentLeft();
        }
        return getLeft();
    }

    @Override
    public int getContentTop() {
        if (mContentPositionDataProvider != null) {
            return mContentPositionDataProvider.getContentTop();
        }
        return getTop();
    }

    @Override
    public int getContentRight() {
        if (mContentPositionDataProvider != null) {
            return mContentPositionDataProvider.getContentRight();
        }
        return getRight();
    }

    @Override
    public int getContentBottom() {
        if (mContentPositionDataProvider != null) {
            return mContentPositionDataProvider.getContentBottom();
        }
        return getBottom();
    }

    /**
     * 外部直接将布局设置进来
     *
     * @param contentView
     */
    public void setContentView(Component contentView) {
        setContentView(contentView, null);
    }

    /**
     * setContentView
     *
     * @param contentView contentView
     * @param lp LayoutConfig
     */
    public void setContentView(Component contentView, StackLayout.LayoutConfig lp) {
        removeAllComponents();
        if (contentView != null) {
            StackLayout.LayoutConfig newLayoutConfig = lp;
            if (lp == null) {
                newLayoutConfig = new StackLayout.LayoutConfig(StackLayout.LayoutConfig.MATCH_PARENT,
                        StackLayout.LayoutConfig.MATCH_PARENT);
            }
            addComponent(contentView, newLayoutConfig);
        }
    }

    /**
     * setContentView
     *
     * @param layoutId layoutId
     */
    public void setContentView(int layoutId) {
        Component child = LayoutScatter.getInstance(getContext()).parse(layoutId, this, false);
        setContentView(child, null);
    }

    /**
     * getOnPagerTitleChangeListener
     *
     * @return mOnPagerTitleChangeListener
     */
    public OnPagerTitleChangeListener getOnPagerTitleChangeListener() {
        return mOnPagerTitleChangeListener;
    }

    /**
     * setOnPagerTitleChangeListener
     *
     * @param onPagerTitleChangeListener onPagerTitleChangeListener
     */
    public void setOnPagerTitleChangeListener(OnPagerTitleChangeListener onPagerTitleChangeListener) {
        mOnPagerTitleChangeListener = onPagerTitleChangeListener;
    }

    /**
     * getContentPositionDataProvider
     *
     * @return mContentPositionDataProvider
     */
    public ContentPositionDataProvider getContentPositionDataProvider() {
        return mContentPositionDataProvider;
    }

    /**
     * setContentPositionDataProvider
     *
     * @param contentPositionDataProvider contentPositionDataProvider
     */
    public void setContentPositionDataProvider(ContentPositionDataProvider contentPositionDataProvider) {
        mContentPositionDataProvider = contentPositionDataProvider;
    }

    /**
     * OnPagerTitleChangeListener interface
     *
     * @since 2020-09-29
     */
    public interface OnPagerTitleChangeListener {
        /**
         * onSelected
         *
         * @param index index index
         * @param totalCount totalCount totalCount
         */
        void onSelected(int index, int totalCount);

        /**
         * onDeselected
         *
         * @param index index
         * @param totalCount totalCount
         */
        void onDeselected(int index, int totalCount);

        /**
         * onLeave
         *
         * @param index index
         * @param totalCount totalCount
         * @param leavePercent leavePercent
         * @param isLeftToRight isLeftToRight
         */
        void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight);

        /**
         * onEnter
         *
         * @param index index
         * @param totalCount totalCount
         * @param enterPercent enterPercent
         * @param isLeftToRight isLeftToRight
         */
        void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight);
    }

    /**
     * ContentPositionDataProvider
     *
     * @since 2020-09-29
     */
    public interface ContentPositionDataProvider {
        /**
         * getContentLeft
         *
         * @return ContentLeft
         */
        int getContentLeft();

        /**
         * getContentTop
         *
         * @return ContentTop
         */
        int getContentTop();

        /**
         * getContentRight
         *
         * @return ContentRight
         */
        int getContentRight();

        /**
         * getContentBottom
         *
         * @return ContentBottom
         */
        int getContentBottom();
    }
}
