package net.lucode.hackware.magicindicator.buildins.commonnavigator.titles;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * 空指示器标题，用于只需要指示器而不需要title的需求
 *
 * @since 2020-09-29
 */
public class DummyPagerTitleView extends Component implements IPagerTitleView {
    /**
     * DummyPagerTitleView
     *
     * @param context context
     */
    public DummyPagerTitleView(Context context) {
        super(context);
    }

    @Override
    public void onSelected(int index, int totalCount) {
    }

    @Override
    public void onDeselected(int index, int totalCount) {
    }

    @Override
    public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
    }

    @Override
    public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
    }
}
