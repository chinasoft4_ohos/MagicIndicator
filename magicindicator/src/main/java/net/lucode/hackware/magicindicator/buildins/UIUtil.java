package net.lucode.hackware.magicindicator.buildins;

import ohos.agp.animation.Animator;
import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * UIUtil
 *
 * @since 2020-09-29
 */
public final class UIUtil {
    private UIUtil() {
    }

    /**
     * vp2px
     *
     * @param context context
     * @param vpValue vpValue
     * @return px
     */
    public static int vp2px(Context context, float vpValue) {
        return AttrHelper.vp2px(vpValue, context);
    }

    /**
     * fp2px
     *
     * @param context context
     * @param fpValue fpValue
     * @return px
     */
    public static int fp2px(Context context, float fpValue) {
        return AttrHelper.fp2px(fpValue, context);
    }

    /**
     * getScreenWidth
     *
     * @param context context
     * @return ScreenWidth
     */
    public static int getScreenWidth(Context context) {
        Point point = new Point();
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        display.getSize(point);
        return point.getPointXToInt();
    }

    /**
     * getInterpolation
     *
     * @param curveType curveType
     * @param input input
     * @return Interpolation
     */
    public static float getInterpolation(int curveType, float input) {
        float result = input;
        switch (curveType) {
            case Animator.CurveType.LINEAR:
                result = input;
                break;
            case Animator.CurveType.ACCELERATE:
                result = (float) Math.pow(input, 2.0f);
                break;
            case Animator.CurveType.DECELERATE:
                result = (float) (1.0f - Math.pow((1.0f - input), 2 * 2.0f));
                break;
            default:
                break;
        }
        return result;
    }
}