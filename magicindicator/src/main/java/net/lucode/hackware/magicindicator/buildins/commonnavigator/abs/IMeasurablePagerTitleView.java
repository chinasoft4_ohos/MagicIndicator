package net.lucode.hackware.magicindicator.buildins.commonnavigator.abs;

/**
 * 可测量内容区域的指示器标题
 *
 * @since 2020-09-29
 */
public interface IMeasurablePagerTitleView extends IPagerTitleView {
    /**
     * getContentLeft
     *
     * @return ContentLeft
     */
    int getContentLeft();

    /**
     * getContentTop
     *
     * @return ContentTop
     */
    int getContentTop();

    /**
     * getContentRight
     *
     * @return ContentRight
     */
    int getContentRight();

    /**
     * getContentBottom
     *
     * @return ContentBottom
     */
    int getContentBottom();
}
