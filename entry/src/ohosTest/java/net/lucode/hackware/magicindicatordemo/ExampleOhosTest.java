/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.lucode.hackware.magicindicatordemo;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.BezierPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.TriangularPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.WrapPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * 单元测试
 *
 * @since 2021-08-11
 */
public class ExampleOhosTest {
    private Context mContext = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().getContext();

    /**
     * 包名测试
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("net.lucode.hackware.magicindicatordemo", actualBundleName);
    }

    /**
     * CommonNavigator测试
     */
    @Test
    public void testCommonNavigator() {
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        assertNotNull(commonNavigator);
    }

    /**
     * SimplePagerTitleView测试
     */
    @Test
    public void testSimplePagerTitleView() {
        SimplePagerTitleView simplePagerTitleView = new SimplePagerTitleView(mContext);
        assertNotNull(simplePagerTitleView);
    }

    /**
     * ClipPagerTitleView测试
     */
    @Test
    public void testClipPagerTitleView() {
        ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(mContext);
        assertNotNull(clipPagerTitleView);
    }

    /**
     * LinePagerIndicator测试
     */
    @Test
    public void testLinePagerIndicator() {
        LinePagerIndicator indicator = new LinePagerIndicator(mContext);
        assertNotNull(indicator);
    }

    /**
     * ColorTransitionPagerTitleView测试
     */
    @Test
    public void testColorTransitionPagerTitleView() {
        ColorTransitionPagerTitleView colorTransitionPagerTitleView = new ColorTransitionPagerTitleView(mContext);
        assertNotNull(colorTransitionPagerTitleView);
    }

    /**
     * BezierPagerIndicator测试
     */
    @Test
    public void testBezierPagerIndicator() {
        BezierPagerIndicator indicator = new BezierPagerIndicator(mContext);
        assertNotNull(indicator);
    }

    /**
     * WrapPagerIndicator测试
     */
    @Test
    public void testWrapPagerIndicator() {
        WrapPagerIndicator indicator = new WrapPagerIndicator(mContext);
        assertNotNull(indicator);
    }

    /**
     * TriangularPagerIndicator测试
     */
    @Test
    public void testTriangularPagerIndicator() {
        TriangularPagerIndicator indicator = new TriangularPagerIndicator(mContext);
        assertNotNull(indicator);
    }
}