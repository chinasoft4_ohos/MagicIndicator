package net.lucode.hackware.magicindicatordemo.ext.titles;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import ohos.app.Context;

/**
 * 颜色渐变和缩放的指示器标题
 *
 * @since 2020-09-29
 */
public class ScaleTransitionPagerTitleView extends ColorTransitionPagerTitleView {
    private float mMinScale = 0.75f;

    /**
     * ScaleTransitionPagerTitleView
     *
     * @param context context
     */
    public ScaleTransitionPagerTitleView(Context context) {
        super(context);
    }

    @Override
    public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
        super.onEnter(index, totalCount, enterPercent, isLeftToRight); // 实现颜色渐变
        setScaleX(mMinScale + (1.0f - mMinScale) * enterPercent);
        setScaleY(mMinScale + (1.0f - mMinScale) * enterPercent);
    }

    @Override
    public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
        super.onLeave(index, totalCount, leavePercent, isLeftToRight); // 实现颜色渐变
        setScaleX(1.0f + (mMinScale - 1.0f) * leavePercent);
        setScaleY(1.0f + (mMinScale - 1.0f) * leavePercent);
    }

    /**
     * getMinScale
     *
     * @return mMinScale
     */
    public float getMinScale() {
        return mMinScale;
    }

    /**
     * setMinScale
     *
     * @param minScale minScale
     */
    public void setMinScale(float minScale) {
        mMinScale = minScale;
    }
}
