package net.lucode.hackware.magicindicatordemo.example;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;
import net.lucode.hackware.magicindicatordemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * DynamicTabExampleAbilitySlice
 *
 * @since 2020-09-29
 */
public class DynamicTabExampleAbilitySlice extends AbilitySlice {
    private static final String[] CHANNELS = new String[]{"CUPCAKE", "DONUT", "ECLAIR",
        "GINGERBREAD", "HONEYCOMB", "ICE_CREAM_SANDWICH", "JELLY_BEAN", "KITKAT", "LOLLIPOP", "M", "NOUGAT"};
    private static final int DELAY_TIME = 100;
    private List<String> mDataList = new ArrayList<String>(Arrays.asList(CHANNELS));
    private ExamplePagerAdapter mExamplePagerAdapter = new ExamplePagerAdapter(mDataList);

    private PageSlider mViewPager;
    private MagicIndicator mMagicIndicator;
    private CommonNavigator mCommonNavigator;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_dynamic_tab_example_layout);
        getWindow().setStatusBarColor(Color.getIntColor("#303f9f"));
        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_view_pager);
        mViewPager.setProvider(mExamplePagerAdapter);

        mMagicIndicator = (MagicIndicator) findComponentById(ResourceTable.Id_magic_indicator1);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#d43d3d")));
        mMagicIndicator.setBackground(shapeElement);
        mCommonNavigator = new CommonNavigator(this);
        mCommonNavigator.setSkimOver(true);
        mCommonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                clipPagerTitleView.setText(mDataList.get(index));
                clipPagerTitleView.setTextColor(Color.getIntColor("#f2c4c4"));
                clipPagerTitleView.setClipColor(Color.WHITE.getValue());
                clipPagerTitleView.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mViewPager.setCurrentPage(index);
                    }
                });
                return clipPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                return null;
            }
        });
        mMagicIndicator.setNavigator(mCommonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);

        findComponentById(ResourceTable.Id_random).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                randomPage(component);
            }
        });
    }

    private void randomPage(Component view) {
        mDataList.clear();
        int total = new SecureRandom().nextInt(CHANNELS.length);
        for (int index = 0; index <= total; index++) {
            mDataList.add(CHANNELS[index]);
        }

        mCommonNavigator.notifyDataSetChanged(); // must call firstly
        mExamplePagerAdapter.notifyDataChanged();
        getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                ToastUtil.showToast(getContext(), "" + mDataList.size() + " page");
            }
        }, DELAY_TIME);
    }

    @Override
    protected void onStop() {
        mViewPager.setCurrentPage(0);
        super.onStop();
    }
}
