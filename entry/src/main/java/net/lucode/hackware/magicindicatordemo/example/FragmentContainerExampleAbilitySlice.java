package net.lucode.hackware.magicindicatordemo.example;

import net.lucode.hackware.magicindicator.FragmentContainerHelper;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;
import net.lucode.hackware.magicindicatordemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.Arrays;
import java.util.List;

/**
 * FragmentContainerExampleAbilitySlice
 *
 * @since 2020-09-29
 */
public class FragmentContainerExampleAbilitySlice extends AbilitySlice {
    private static final String[] CHANNELS = new String[]{"KITKAT", "NOUGAT", "DONUT"};
    private List<String> mDataList = Arrays.asList(CHANNELS);
    private FragmentContainerHelper mFragmentContainerHelper = new FragmentContainerHelper();
    private Text mText;
    private ExamplePagerAdapter mExamplePagerAdapter = new ExamplePagerAdapter(mDataList);

    private PageSlider mViewPager;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fragment_container_example_layout);
        getWindow().setStatusBarColor(Color.getIntColor("#303f9f"));
        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_view_pager);
        mViewPager.setProvider(mExamplePagerAdapter);
        initMagicIndicator();
        mFragmentContainerHelper.handlePageSelected(1, false);
        mText = (Text) findComponentById(ResourceTable.Id_container);
        mText.setText(CHANNELS[1]);
        mViewPager.setCurrentPage(1);
    }

    private void initMagicIndicator() {
        MagicIndicator magicIndicator = (MagicIndicator) findComponentById(ResourceTable.Id_magic_indicator1);
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_round_indicator_bg);
        magicIndicator.setBackground(background);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return CHANNELS.length;
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                clipPagerTitleView.setText(CHANNELS[index]);
                clipPagerTitleView.setTextColor(Color.getIntColor("#e94220"));
                clipPagerTitleView.setClipColor(Color.WHITE.getValue());
                clipPagerTitleView.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mFragmentContainerHelper.handlePageSelected(index);
                        mText.setText(CHANNELS[index]);
                    }
                });
                return clipPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                float navigatorHeight = UIUtil.vp2px(context, 45);
                float borderWidth = UIUtil.vp2px(context, 1);
                float lineHeight = navigatorHeight - 2 * borderWidth;
                indicator.setLineHeight(lineHeight);
                indicator.setRoundRadius(lineHeight / 2);
                indicator.setYOffset(borderWidth);
                indicator.setColors(Color.getIntColor("#bc2a2a"));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        mFragmentContainerHelper.attachMagicIndicator(magicIndicator);
    }

    @Override
    protected void onStop() {
        mViewPager.setCurrentPage(0);
        super.onStop();
    }
}
