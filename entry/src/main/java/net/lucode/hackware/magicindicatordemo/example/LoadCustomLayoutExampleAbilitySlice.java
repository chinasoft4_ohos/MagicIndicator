package net.lucode.hackware.magicindicatordemo.example;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import net.lucode.hackware.magicindicatordemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.Arrays;
import java.util.List;

/**
 * LoadCustomLayoutExampleAbilitySlice
 *
 * @since 2020-09-29
 */
public class LoadCustomLayoutExampleAbilitySlice extends AbilitySlice {
    private static final String[] CHANNELS = new String[]{"NOUGAT", "DONUT", "ECLAIR", "KITKAT"};
    private List<String> mDataList = Arrays.asList(CHANNELS);
    private ExamplePagerAdapter mExamplePagerAdapter = new ExamplePagerAdapter(mDataList);

    private PageSlider mViewPager;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_load_custom_layout_example);
        getWindow().setStatusBarColor(Color.getIntColor("#303f9f"));
        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_view_pager);
        mViewPager.setProvider(mExamplePagerAdapter);
        initMagicIndicator1();
    }

    private void initMagicIndicator1() {
        MagicIndicator magicIndicator = (MagicIndicator) findComponentById(ResourceTable.Id_magic_indicator1);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        magicIndicator.setBackground(shapeElement);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                CommonPagerTitleView commonPagerTitleView = new CommonPagerTitleView(context);

                // load custom layout
                Component customLayout = LayoutScatter.getInstance(getContext())
                        .parse(ResourceTable.Layout_simple_pager_title_layout, null, false);
                final Image titleImg = (Image) customLayout.findComponentById(ResourceTable.Id_title_img);
                final Text titleText = (Text) customLayout.findComponentById(ResourceTable.Id_title_text);
                titleImg.setPixelMap(ResourceTable.Media_icon);
                titleText.setText(mDataList.get(index));
                commonPagerTitleView.setContentView(customLayout);

                commonPagerTitleView.setOnPagerTitleChangeListener(
                        new CommonPagerTitleView.OnPagerTitleChangeListener() {
                            @Override
                            public void onSelected(int index, int totalCount) {
                                titleText.setTextColor(Color.WHITE);
                            }

                            @Override
                            public void onDeselected(int index, int totalCount) {
                                titleText.setTextColor(Color.LTGRAY);
                            }

                            @Override
                            public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
                                titleImg.setScaleX(1.3f + (0.8f - 1.3f) * leavePercent);
                                titleImg.setScaleY(1.3f + (0.8f - 1.3f) * leavePercent);
                            }

                            @Override
                            public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
                                titleImg.setScaleX(0.8f + (1.3f - 0.8f) * enterPercent);
                                titleImg.setScaleY(0.8f + (1.3f - 0.8f) * enterPercent);
                            }
                        });
                commonPagerTitleView.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mViewPager.setCurrentPage(index);
                    }
                });
                return commonPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                return null;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);

    }

    @Override
    protected void onStop() {
        mViewPager.setCurrentPage(0);
        super.onStop();
    }
}
