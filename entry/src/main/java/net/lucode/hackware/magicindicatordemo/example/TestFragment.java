package net.lucode.hackware.magicindicatordemo.example;

import net.lucode.hackware.magicindicatordemo.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * TestFragment
 *
 * @since 2020-09-29
 */
public class TestFragment extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter inflater, ComponentContainer container, Intent intent) {
        Component mComponent = inflater.parse(ResourceTable.Layout_simple_fragment_layout, container, false);
        Text textView = (Text) mComponent.findComponentById(ResourceTable.Id_text_view);
        textView.setText("");
        return mComponent;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }
}
