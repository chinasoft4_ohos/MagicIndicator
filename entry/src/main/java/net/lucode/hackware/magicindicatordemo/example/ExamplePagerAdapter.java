package net.lucode.hackware.magicindicatordemo.example;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;

import java.util.List;

/**
 * ExamplePagerAdapter
 *
 * @since 2020-09-29
 */
public class ExamplePagerAdapter extends PageSliderProvider {
    private List<String> mDataList;

    /**
     * ExamplePagerAdapter
     *
     * @param dataList dataList
     */
    public ExamplePagerAdapter(List<String> dataList) {
        mDataList = dataList;
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Text textView = new Text(componentContainer.getContext());
        textView.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        textView.setHeight(ComponentContainer.LayoutConfig.MATCH_PARENT);
        textView.setText(mDataList.get(position));
        textView.setTextAlignment(TextAlignment.CENTER);
        textView.setTextColor(Color.BLACK);
        textView.setTextSize(24, Text.TextSizeType.FP);
        componentContainer.addComponent(textView);
        return textView;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object object) {
        componentContainer.removeComponent((Component) object);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }

    @Override
    public int getPageIndex(Object object) {
        Text textView = (Text) object;
        String text = textView.getText().toString();
        int index = mDataList.indexOf(text);
        if (index >= 0) {
            return index;
        }
        return POSITION_INVALID;
    }

    @Override
    public String getPageTitle(int position) {
        return mDataList.get(position);
    }
}
