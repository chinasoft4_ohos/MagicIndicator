package net.lucode.hackware.magicindicatordemo.ext.indicators;

import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.model.PositionData;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

/**
 * 手指跟随的小圆点指示器
 *
 * @since 2020-09-29
 */
public class DotPagerIndicator extends Component implements IPagerIndicator, Component.DrawTask {
    private List<PositionData> mDataList;
    private float mRadius;
    private float mYOffset;
    private int mDotColor;

    private float mCircleCenterX;
    private Paint mPaint = new Paint();

    /**
     * DotPagerIndicator
     *
     * @param context context
     */
    public DotPagerIndicator(Context context) {
        super(context);
        mRadius = UIUtil.vp2px(context, 3);
        mYOffset = UIUtil.vp2px(context, 3);
        mDotColor = Color.WHITE.getValue();
        addDrawTask(this);
    }

    @Override
    public void onPageSelected(int position) {
        if (mDataList == null || mDataList.isEmpty()) {
            return;
        }
        PositionData data = mDataList.get(position);
        mCircleCenterX = data.mLeft + data.width() / 2;
        invalidate();
    }

    @Override
    public void onPositionDataProvide(List<PositionData> dataList) {
        mDataList = dataList;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mPaint.setColor(new Color(mDotColor));
        canvas.drawCircle(mCircleCenterX, getHeight() - mYOffset - mRadius, mRadius, mPaint);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    /**
     * getRadius
     *
     * @return mRadius
     */
    public float getRadius() {
        return mRadius;
    }

    /**
     * setRadius
     *
     * @param radius radius
     */
    public void setRadius(float radius) {
        mRadius = radius;
        invalidate();
    }

    /**
     * getYOffset
     *
     * @return mYOffset
     */
    public float getYOffset() {
        return mYOffset;
    }

    /**
     * setYOffset
     *
     * @param yOffset yOffset
     */
    public void setYOffset(float yOffset) {
        mYOffset = yOffset;
        invalidate();
    }

    /**
     * getDotColor
     *
     * @return mDotColor
     */
    public int getDotColor() {
        return mDotColor;
    }

    /**
     * setDotColor
     *
     * @param dotColor dotColor
     */
    public void setDotColor(int dotColor) {
        mDotColor = dotColor;
        invalidate();
    }
}
