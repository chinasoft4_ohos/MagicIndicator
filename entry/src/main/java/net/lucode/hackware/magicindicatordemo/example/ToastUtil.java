/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.lucode.hackware.magicindicatordemo.example;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.DirectionalLayout.LayoutConfig;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * 吐司工具类
 *
 * @since 2021-08-10
 */
public class ToastUtil {
    private static final int TEXT_SIZE = 36;
    private static final int BACKGROUND_COLOR = 0xF0F0F0F0;
    private static final float BACKGROUND_RADIUS = 50.0F;
    private static final int LAYOUT_MARGIN_BOTTOM = 100;
    private static final int LAYOUT_PADDING = 30;
    private static final float LAYOUT_SCALE = 0;
    private static final long ANIMATOR_DURATION = 300L;
    private static final int TOAST_DURATION = 2000;
    private static ToastDialog mToastDialog;

    private ToastUtil() {
    }

    /**
     * 显示一个吐司
     *
     * @param abilityContext 上下文
     * @param str 文本
     * @return ToastDialog
     */
    public static ToastDialog showToast(Context abilityContext, String str) {
        if (mToastDialog != null) {
            mToastDialog.cancel();
        }
        Text text = new Text(abilityContext);
        text.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        text.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        text.setTextSize(TEXT_SIZE);
        text.setText(str);
        text.setMultipleLine(true);
        text.setPadding(0, 0, LAYOUT_PADDING + LAYOUT_PADDING, 0);
        text.setTextAlignment(TextAlignment.CENTER);

        DirectionalLayout directionalLayout = new DirectionalLayout(abilityContext);
        directionalLayout.setBackground(createDrawable(BACKGROUND_COLOR, BACKGROUND_RADIUS));
        LayoutConfig layoutConfig = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        layoutConfig.setMarginBottom(LAYOUT_MARGIN_BOTTOM);
        directionalLayout.setLayoutConfig(layoutConfig);
        directionalLayout.setPadding(LAYOUT_PADDING, LAYOUT_PADDING, LAYOUT_PADDING, LAYOUT_PADDING);
        directionalLayout.setScale(LAYOUT_SCALE, LAYOUT_SCALE);

        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setDuration(ANIMATOR_DURATION);
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            directionalLayout.setScale(v, v);
        });
        animatorValue.start();

        directionalLayout.addComponent(text);
        mToastDialog = new ToastDialog(abilityContext);
        mToastDialog.setContentCustomComponent(directionalLayout)
                .setDuration(TOAST_DURATION)
                .setAutoClosable(true)
                .setAlignment(LayoutAlignment.BOTTOM)
                .setTransparent(true)
                .show();
        return mToastDialog;
    }

    private static ShapeElement createDrawable(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }
}
