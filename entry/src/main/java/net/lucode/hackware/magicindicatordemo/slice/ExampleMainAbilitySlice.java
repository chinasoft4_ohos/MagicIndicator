package net.lucode.hackware.magicindicatordemo.slice;

import net.lucode.hackware.magicindicatordemo.ResourceTable;
import net.lucode.hackware.magicindicatordemo.example.BadgeTabExampleAbilitySlice;
import net.lucode.hackware.magicindicatordemo.example.CustomNavigatorExampleAbilitySlice;
import net.lucode.hackware.magicindicatordemo.example.DynamicTabExampleAbilitySlice;
import net.lucode.hackware.magicindicatordemo.example.FixedTabExampleAbilitySlice;
import net.lucode.hackware.magicindicatordemo.example.FragmentContainerExampleAbilitySlice;
import net.lucode.hackware.magicindicatordemo.example.LoadCustomLayoutExampleAbilitySlice;
import net.lucode.hackware.magicindicatordemo.example.NoTabOnlyIndicatorExampleAbilitySlice;
import net.lucode.hackware.magicindicatordemo.example.ScrollableTabExampleAbilitySlice;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

/**
 * ExampleMainAbilitySlice
 *
 * @since 2020-09-29
 */
public class ExampleMainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_example_main_layout);
        getWindow().setStatusBarColor(Color.getIntColor("#303f9f"));

        findComponentById(ResourceTable.Id_scrollable_tab).setClickedListener(this);
        findComponentById(ResourceTable.Id_fixed_tab).setClickedListener(this);
        findComponentById(ResourceTable.Id_dynamic_tab).setClickedListener(this);
        findComponentById(ResourceTable.Id_no_tab_only_indicator).setClickedListener(this);
        findComponentById(ResourceTable.Id_tab_with_badge_view).setClickedListener(this);
        findComponentById(ResourceTable.Id_work_with_fragment_container).setClickedListener(this);
        findComponentById(ResourceTable.Id_load_custom_layout).setClickedListener(this);
        findComponentById(ResourceTable.Id_custom_navigator).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        Intent intent = new Intent();
        switch (component.getId()) {
            case ResourceTable.Id_scrollable_tab:
                present(new ScrollableTabExampleAbilitySlice(), intent);
                break;
            case ResourceTable.Id_fixed_tab:
                present(new FixedTabExampleAbilitySlice(), intent);
                break;
            case ResourceTable.Id_dynamic_tab:
                present(new DynamicTabExampleAbilitySlice(), intent);
                break;
            case ResourceTable.Id_no_tab_only_indicator:
                present(new NoTabOnlyIndicatorExampleAbilitySlice(), intent);
                break;
            case ResourceTable.Id_tab_with_badge_view:
                present(new BadgeTabExampleAbilitySlice(), intent);
                break;
            case ResourceTable.Id_work_with_fragment_container:
                present(new FragmentContainerExampleAbilitySlice(), intent);
                break;
            case ResourceTable.Id_load_custom_layout:
                present(new LoadCustomLayoutExampleAbilitySlice(), intent);
                break;
            case ResourceTable.Id_custom_navigator:
                present(new CustomNavigatorExampleAbilitySlice(), intent);
                break;
            default:
                break;
        }
    }
}
