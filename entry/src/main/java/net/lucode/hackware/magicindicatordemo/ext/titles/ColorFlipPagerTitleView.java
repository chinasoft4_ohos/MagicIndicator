package net.lucode.hackware.magicindicatordemo.ext.titles;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * ColorFlipPagerTitleView
 *
 * @since 2020-09-29
 */
public class ColorFlipPagerTitleView extends SimplePagerTitleView {
    private float mChangePercent = 0.5f;

    /**
     * ColorFlipPagerTitleView
     *
     * @param context context
     */
    public ColorFlipPagerTitleView(Context context) {
        super(context);
    }

    @Override
    public void onLeave(int index, int totalCount, float leavePercent, boolean isLeftToRight) {
        if (leavePercent >= mChangePercent) {
            setTextColor(new Color(mNormalColor));
        } else {
            setTextColor(new Color(mSelectedColor));
        }
    }

    @Override
    public void onEnter(int index, int totalCount, float enterPercent, boolean isLeftToRight) {
        if (enterPercent >= mChangePercent) {
            setTextColor(new Color(mSelectedColor));
        } else {
            setTextColor(new Color(mNormalColor));
        }
    }

    @Override
    public void onSelected(int index, int totalCount) {
    }

    @Override
    public void onDeselected(int index, int totalCount) {
    }

    /**
     * getChangePercent
     *
     * @return mChangePercent
     */
    public float getChangePercent() {
        return mChangePercent;
    }

    /**
     * setChangePercent
     *
     * @param changePercent changePercent
     */
    public void setChangePercent(float changePercent) {
        mChangePercent = changePercent;
    }
}
