package net.lucode.hackware.magicindicatordemo.example;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgeAnchor;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgeRule;
import net.lucode.hackware.magicindicatordemo.ResourceTable;
import net.lucode.hackware.magicindicatordemo.ext.titles.ScaleTransitionPagerTitleView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.Arrays;
import java.util.List;

/**
 * BadgeTabExampleAbilitySlice
 *
 * @since 2020-09-29
 */
public class BadgeTabExampleAbilitySlice extends AbilitySlice {
    private static final String[] CHANNELS = new String[]{"KITKAT", "NOUGAT", "DONUT"};
    private List<String> mDataList = Arrays.asList(CHANNELS);
    private ExamplePagerAdapter mExamplePagerAdapter = new ExamplePagerAdapter(mDataList);

    private PageSlider mViewPager;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_badge_tab_example_layout);
        getWindow().setStatusBarColor(Color.getIntColor("#303f9f"));
        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_view_pager);
        mViewPager.setProvider(mExamplePagerAdapter);
        initMagicIndicator1();
        initMagicIndicator2();
        initMagicIndicator3();
        initMagicIndicator4();
    }

    private void initMagicIndicator1() {
        MagicIndicator magicIndicator = (MagicIndicator) findComponentById(ResourceTable.Id_magic_indicator1);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                BadgePagerTitleView badgePagerView = new BadgePagerTitleView(context);
                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setNormalColor(Color.getIntColor("#88ffffff"));
                simplePagerTitleView.setSelectedColor(Color.WHITE.getValue());
                if (index == 0) {
                    simplePagerTitleView.showDividers(false);
                } else {
                    simplePagerTitleView.showDividers(true);
                }
                simplePagerTitleView.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mViewPager.setCurrentPage(index);
                        badgePagerView.setBadgeView(null); // cancel badge when click tab
                    }
                });
                badgePagerView.setInnerPagerTitleView(simplePagerTitleView);

                // setup badge
                if (index != 2) {
                    Text badgeTextView = (Text) LayoutScatter.getInstance(context)
                            .parse(ResourceTable.Layout_simple_count_badge_layout, null, false);
                    badgeTextView.setText(" " + (index + 1) + " ");
                    badgePagerView.setBadgeView(badgeTextView);
                } else {
                    Component badgeImageView = LayoutScatter.getInstance(context)
                            .parse(ResourceTable.Layout_simple_red_dot_badge_layout, null, false);
                    badgePagerView.setBadgeView(badgeImageView);
                }

                // set badge position
                if (index == 0) {
                    badgePagerView.setXBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_LEFT, UIUtil.vp2px(context, -15)));
                    badgePagerView.setYBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_TOP, -10));
                } else if (index == 1) {
                    badgePagerView.setXBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_RIGHT, UIUtil.vp2px(context, 10)));
                    badgePagerView.setYBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_TOP, -10));
                } else if (index == 2) {
                    badgePagerView.setXBadgeRule(new BadgeRule(BadgeAnchor.CENTER_X, 0));
                    badgePagerView.setYBadgeRule(new BadgeRule(BadgeAnchor.CENTER_Y, UIUtil.vp2px(context, 10)));
                }

                // don't cancel badge when tab selected
                badgePagerView.setAutoCancelBadge(false);
                return badgePagerView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(Color.getIntColor("#40c4ff"));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    private void initMagicIndicator2() {
        MagicIndicator magicIndicator = (MagicIndicator) findComponentById(ResourceTable.Id_magic_indicator2);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
        magicIndicator.setBackground(shapeElement);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ScaleTransitionPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setTextSize(16, Text.TextSizeType.FP);
                simplePagerTitleView.setNormalColor(Color.getIntColor("#616161"));
                simplePagerTitleView.setSelectedColor(Color.getIntColor("#f57c00"));
                simplePagerTitleView.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mViewPager.setCurrentPage(index);
                    }
                });
                BadgePagerTitleView badgePagerView = new BadgePagerTitleView(context);
                badgePagerView.setInnerPagerTitleView(simplePagerTitleView);

                // setup badge
                if (index == 1) {
                    Component badgeImageView = (Component) LayoutScatter.getInstance(context)
                            .parse(ResourceTable.Layout_simple_red_dot_badge_layout, null, false);
                    badgePagerView.setBadgeView(badgeImageView);
                    badgePagerView.setXBadgeRule(new BadgeRule(BadgeAnchor.CENTER_X, 0));
                    badgePagerView.setYBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_BOTTOM, UIUtil.vp2px(context, 5)));
                }

                // cancel badge when click tab, default true
                badgePagerView.setAutoCancelBadge(true);

                return badgePagerView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setStartInterpolator(Animator.CurveType.ACCELERATE);
                indicator.setEndInterpolator(Animator.CurveType.DECELERATE);
                indicator.setYOffset(UIUtil.vp2px(context, 39));
                indicator.setLineHeight(UIUtil.vp2px(context, 1));
                indicator.setColors(Color.getIntColor("#f57c00"));
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                if (index == 0) {
                    return 2.0f;
                } else if (index == 1) {
                    return 1.2f;
                } else {
                    return 1.0f;
                }
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    private void initMagicIndicator3() {
        MagicIndicator magicIndicator = (MagicIndicator) findComponentById(ResourceTable.Id_magic_indicator3);
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_round_indicator_bg);
        magicIndicator.setBackground(background);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                clipPagerTitleView.setText(mDataList.get(index));
                clipPagerTitleView.setTextColor(Color.getIntColor("#e94220"));
                clipPagerTitleView.setClipColor(Color.WHITE.getValue());
                clipPagerTitleView.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mViewPager.setCurrentPage(index);
                    }
                });
                BadgePagerTitleView badgePagerTitleView = new BadgePagerTitleView(context);
                badgePagerTitleView.setInnerPagerTitleView(clipPagerTitleView);
                return badgePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                float navigatorHeight = UIUtil.vp2px(context, 45);
                float borderWidth = UIUtil.vp2px(context, 2);
                float lineHeight = navigatorHeight - 2 * borderWidth;
                indicator.setLineHeight(lineHeight);
                indicator.setRoundRadius(lineHeight / 2);
                indicator.setYOffset(borderWidth);
                indicator.setColors(Color.getIntColor("#bc2a2a"));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    private void initMagicIndicator4() {
        MagicIndicator magicIndicator = (MagicIndicator) findComponentById(ResourceTable.Id_magic_indicator4);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                simplePagerTitleView.setNormalColor(Color.GRAY.getValue());
                simplePagerTitleView.setSelectedColor(Color.WHITE.getValue());
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mViewPager.setCurrentPage(index);
                    }
                });
                BadgePagerTitleView badgePagerTitleView = new BadgePagerTitleView(context);
                badgePagerTitleView.setInnerPagerTitleView(simplePagerTitleView);
                return badgePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setColors(Color.WHITE.getValue());
                return linePagerIndicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        DirectionalLayout titleContainer = commonNavigator.getTitleContainer(); // must after setNavigator
        titleContainer.setBackground(new ShapeElement() {
            @Override
            public int getWidth() {
                return UIUtil.vp2px(BadgeTabExampleAbilitySlice.this, 15);
            }
        });
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    @Override
    protected void onStop() {
        mViewPager.setCurrentPage(0);
        super.onStop();
    }
}
