package net.lucode.hackware.magicindicatordemo.ext.indicators;

import net.lucode.hackware.magicindicator.FragmentContainerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.model.PositionData;
import ohos.agp.animation.Animator;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Rect;
import ohos.app.Context;

import java.util.List;

/**
 * 通用的indicator，支持外面设置Drawable
 *
 * @since 2020-09-29
 */
public class CommonPagerIndicator extends Component implements IPagerIndicator, Component.DrawTask {
    private static final int MODE_MATCH_EDGE = 0; // drawable宽度 == title宽度 - 2 * mXOffset
    private static final int MODE_WRAP_CONTENT = 1; // drawable宽度 == title内容宽度 - 2 * mXOffset
    private static final int MODE_EXACTLY = 2;

    private int mMode; // 默认为MODE_MATCH_EDGE模式
    private ShapeElement mIndicatorDrawable;

    // 控制动画
    private int mStartInterpolator = Animator.CurveType.LINEAR;
    private int mEndInterpolator = Animator.CurveType.LINEAR;

    private float mDrawableHeight;
    private float mDrawableWidth;
    private float mYOffset;
    private float mXOffset;

    private List<PositionData> mPositionDataList;
    private Rect mDrawableRect = new Rect();

    /**
     * CommonPagerIndicator
     *
     * @param context context
     */
    public CommonPagerIndicator(Context context) {
        super(context);
        addDrawTask(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mIndicatorDrawable == null) {
            return;
        }

        if (mPositionDataList == null || mPositionDataList.isEmpty()) {
            return;
        }
        int nextPosition = position + 1;
        if (positionOffsetPixels < 0) {
            nextPosition = position - 1;
        }

        // 计算锚点位置
        PositionData current = FragmentContainerHelper.getImitativePositionData(mPositionDataList, position);
        PositionData next = FragmentContainerHelper.getImitativePositionData(mPositionDataList, nextPosition);

        float leftX;
        float nextLeftX;
        float rightX;
        float nextRightX;
        if (mMode == MODE_MATCH_EDGE) {
            leftX = current.mLeft + mXOffset;
            nextLeftX = next.mLeft + mXOffset;
            rightX = current.mRight - mXOffset;
            nextRightX = next.mRight - mXOffset;
            mDrawableRect.top = (int) mYOffset;
            mDrawableRect.bottom = (int) (getHeight() - mYOffset);
        } else if (mMode == MODE_WRAP_CONTENT) {
            leftX = current.mContentLeft + mXOffset;
            nextLeftX = next.mContentLeft + mXOffset;
            rightX = current.mContentRight - mXOffset;
            nextRightX = next.mContentRight - mXOffset;
            mDrawableRect.top = (int) (current.mContentTop - mYOffset);
            mDrawableRect.bottom = (int) (current.mContentBottom + mYOffset);
        } else { // MODE_EXACTLY
            leftX = current.mLeft + (current.width() - mDrawableWidth) / 2;
            nextLeftX = next.mLeft + (next.width() - mDrawableWidth) / 2;
            rightX = current.mLeft + (current.width() + mDrawableWidth) / 2;
            nextRightX = next.mLeft + (next.width() + mDrawableWidth) / 2;
            mDrawableRect.top = (int) (getHeight() - mDrawableHeight - mYOffset);
            mDrawableRect.bottom = (int) (getHeight() - mYOffset);
        }

        mDrawableRect.left = (int) (leftX + (nextLeftX - leftX) * positionOffset);
        mDrawableRect.right = (int) (rightX + (nextRightX - rightX) * positionOffset);
        mIndicatorDrawable.setBounds(mDrawableRect);

        invalidate();
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mIndicatorDrawable != null) {
            mIndicatorDrawable.drawToCanvas(canvas);
        }
    }

    @Override
    public void onPositionDataProvide(List<PositionData> dataList) {
        mPositionDataList = dataList;
    }

    /**
     * getIndicatorDrawable
     *
     * @return mIndicatorDrawable
     */
    public ShapeElement getIndicatorDrawable() {
        return mIndicatorDrawable;
    }

    /**
     * setIndicatorDrawable
     *
     * @param indicatorDrawable indicatorDrawable
     */
    public void setIndicatorDrawable(ShapeElement indicatorDrawable) {
        mIndicatorDrawable = indicatorDrawable;
    }

    /**
     * getStartInterpolator
     *
     * @return mStartInterpolator
     */
    public int getStartInterpolator() {
        return mStartInterpolator;
    }

    /**
     * setStartInterpolator
     *
     * @param startInterpolator startInterpolator
     */
    public void setStartInterpolator(int startInterpolator) {
        mStartInterpolator = startInterpolator;
    }

    /**
     * getEndInterpolator
     *
     * @return mEndInterpolator
     */
    public int getEndInterpolator() {
        return mEndInterpolator;
    }

    /**
     * setEndInterpolator
     *
     * @param endInterpolator endInterpolator
     */
    public void setEndInterpolator(int endInterpolator) {
        mEndInterpolator = endInterpolator;
    }

    /**
     * getMode
     *
     * @return mMode
     */
    public int getMode() {
        return mMode;
    }

    /**
     * setMode
     *
     * @param mode mode
     */
    public void setMode(int mode) {
        if (mode == MODE_EXACTLY || mode == MODE_MATCH_EDGE || mode == MODE_WRAP_CONTENT) {
            mMode = mode;
        }
    }

    /**
     * getDrawableHeight
     *
     * @return mDrawableHeight
     */
    public float getDrawableHeight() {
        return mDrawableHeight;
    }

    /**
     * setDrawableHeight
     *
     * @param drawableHeight drawableHeight
     */
    public void setDrawableHeight(float drawableHeight) {
        mDrawableHeight = drawableHeight;
    }

    /**
     * getDrawableWidth
     *
     * @return mDrawableWidth
     */
    public float getDrawableWidth() {
        return mDrawableWidth;
    }

    /**
     * setDrawableWidth
     *
     * @param drawableWidth drawableWidth
     */
    public void setDrawableWidth(float drawableWidth) {
        mDrawableWidth = drawableWidth;
    }

    /**
     * getYOffset
     *
     * @return mYOffset
     */
    public float getYOffset() {
        return mYOffset;
    }

    /**
     * setYOffset
     *
     * @param yOffset yOffset
     */
    public void setYOffset(float yOffset) {
        mYOffset = yOffset;
    }

    /**
     * getXOffset
     *
     * @return mXOffset
     */
    public float getXOffset() {
        return mXOffset;
    }

    /**
     * setXOffset
     *
     * @param xOffset xOffset
     */
    public void setXOffset(float xOffset) {
        mXOffset = xOffset;
    }
}
